var localFuncs = {

};

var localVars = {
    alphabet: ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
               "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"],

    htmlElements: {

        multiple_choice: {
            questionItem:
                '<div class="container col-sm-12 questionItem questionMultipleChoice">' +
                    '<div class="row">' +
                        '<div class="col-sm-2">' +
                            '<span class="questionIndex"></span>' +
                        '</div>' +
                        '<div class="col-sm-10">' +
                            '<div class="form-group">' +
                                '<label class="col-sm-2 control-label">Title</label>' +
                                '<div class="col-sm-9">' +
                                    '<input class="form-control questionTitle" type="text" name="questionTitle" placeholder="Fill your question" />' +
                                '</div>' +
                                '<div class="col-sm-1">' +
                                    '<button type="button" class="btn btn-default btn-xs removeQuestionButton">' +
                                        '<span class="glyphicon glyphicon-remove"></span>' +
                                    '</button>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="row">' +
                        '<div class="col-sm-12">' +
                            '<div class="panel panel-default">' +
                                '<div class="panel-heading text-center">Possible answers</div>' +
                                '<table class="table questionAnswers">' +
                                    '<tr>' +
                                        '<th>#</th>' +
                                        '<th>Answer</th>' +
                                        '<th>Correct ?</th>' +
                                        '<th>' +

                                            '<button type="button" class="btn btn-success btn-xs addAnswerButton">' +
                                                '<span class="glyphicon glyphicon-plus"></span>' +
                                            '</button>' +

                                        '</th>' +
                                    '</tr>' +
                                    '<tr class="hidden answerAnchor"></tr>' +
                                '</table>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>',

            answerItem:

                '<tr class="answerItem answerMultipleChoice">' +
                    '<td class="answerIndex">A</td>' +
                    '<td>' +
                        '<input type="text" name="answerTitle" class="form-control answerTitle" />' +
                    '</td>' +
                    '<td>' +
                        '<select class="form-control answerCorrect" name="answerCorrect">' +
                            '<option value="true">True</option>' +
                            '<option value="false">False</option>' +
                        '</select>' +
                    '</td>' +
                    '<td>' +
                        '<button type="button" class="btn btn-danger btn-xs removeAnswerButton">' +
                            '<span class="glyphicon glyphicon-minus"></span>' +
                        '</button>' +
                    '</td>' +
                '</tr>'

        },



        words: {
            questionItem:
                '<div class="container col-sm-12 questionItem questionWords">' +
                    '<div class="row">' +
                        '<div class="col-sm-2">' +
                            '<span class="questionIndex"></span>' +
                        '</div>' +
                        '<div class="col-sm-10">' +
                            '<div class="form-group">' +
                                '<label class="col-sm-2 control-label">Title</label>' +
                                '<div class="col-sm-9">' +
                                    '<input class="form-control questionTitle" type="text" name="questionTitle" placeholder="Fill your question" />' +
                                '</div>' +
                                '<div class="col-sm-1">' +
                                    '<button type="button" class="btn btn-default btn-xs removeQuestionButton">' +
                                        '<span class="glyphicon glyphicon-remove"></span>' +
                                    '</button>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="row">' +
                        '<div class="col-sm-12">' +
                            '<div class="panel panel-default">' +
                                '<div class="panel-heading text-center">Answer</div>' +
                                '<div class="panel-body">' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>',

            answerItem: ''
        },



        other: {
            questionItem: '',
            answerItem: ''
        }
    }

};


CqQcFuncs = {


    quiz: {

        /**
         * UPDATES THE QUESTIONS' INDEX
         * CALLED AT EACH QUESTION INSERTION/DELETION
         */
        updateQuestionsIndex: function () {

            $("#quizQuestions")
                .children(".questionItem")
                .each(function (index) {
                    $(this).find("span.questionIndex")
                        .text("QUESTION " + (index + 1));
                });

        },



        /** INSIDE MULTIPLE-CHOICE QUESTION: UPDATES THE ANSWERS' INDEX.
         *  CALLED AT EACH ANSWER INSERTION/DELETION
         * @param {Jquery Object} questionAnswersTable [[Represents the table containing all the answer items]]
         */
        updateAnswersIndex: function (questionAnswersTable) {
            questionAnswersTable.find("tr.answerItem")
                .each(function (index) {
                    $(this).find("td.answerIndex").text(localVars.alphabet[index]);
                });
        },



        /**
         * SCANS THE DOM DOCUMENT TO GENERATE A QUIZ OBJECT
         * @returns {Object} [[The Quiz object that is later inserted in the DB]]
         */
        scanQuizForm: function () {
            var quiz = new CQ_Constructs.QuizItem();
            quiz.title = $("form.quizForm").find("[name=quizTitle]").val();
            quiz.subtitle = $("form.quizForm").find("[name=quizSubtitle]").val();
            quiz.codePrefix = $("form.quizForm").find("[name=codePrefix]").val();

    //        Scan the questions
            $("#quizQuestions")
                .children(".questionItem")
                .each(function (qIndex, q) {

                    var newQ;

//                    [[TYPED-CONTENT]]
    //				[[MULTIPLE-CHOICE QUESTION]]
                    if($(q).hasClass("questionMultipleChoice")) {

                        newQ = new CQ_Constructs.QuestionItem("multiple_choice");
                        newQ.qTitle = $(q).find("[name=questionTitle]").val();

                        //            Scan the answers
                        $(q).find("table.questionAnswers")
                            .find(".answerItem")
                            .each(function (aIndex, a) {
                            var newA = new CQ_Constructs.AnswerItem("multiple_choice");
                            newA.aLetter = $(a).find("td.answerIndex").text();
                            newA.aTitle = $(a).find("[name=answerTitle]").val();

                            // To store the value as a Boolean (lighter) rather than a String
                            if($(a).find("[name=answerCorrect]").val() === "true") {
                                newA.aCorrect = true;
                            } else {
                                newA.aCorrect = false;
                            }
                            newQ.qAnswers.push(newA);
                        });



    //				[[WORDS QUESTION]]
                    } else if($(q).hasClass("questionWords")) {

                        newQ = new CQ_Constructs.QuestionItem("words");
                        newQ.qTitle = $(q).find("[name=questionTitle]").val();

    //            		Scan the answer


    //				[[OTHER QUESTION]]
                    } else if($(q).hasClass("questionOther")) {

                    }

                    quiz.questions.push(newQ);

                });

            return quiz;
        },



        /**
         * GENERATES A JQUERY OBJECT THAT REPRESENTS A QUESTION ELEMENT
         * @param   {String}        type          [[The question type requested. A dbValue string must be used. See cqCommonFuncs.js, localVars.questionTypes]]
         * @param   {Object}        data          [[If defined, it is used to feed the element (fill the inputs)]]
         * @returns {Jquery Object}
         */
        createQuestionItem: function(type, data) {

            var $newQuestionItem;

//            [[TYPED-CONTENT]]
            switch(type) {

    //			[[MULTIPLE-CHOICE QUESTION]]
                case "multiple_choice":

                    $newQuestionItem = $($.parseHTML(localVars.htmlElements.multiple_choice.questionItem));


                    if(data) {
                        $newQuestionItem
                            .find("[name=questionTitle]")
                            .val(data.qTitle);


                    } else {
    /*					If no data is defined, it then means that we're in a Quiz Creation form and
                        consequently the questionItem needs an default answerItem without a removeAnswerButton */
                        var $defaultAnswerItem = CqQcFuncs.quiz.createAnswerItem(type, true);
                        $defaultAnswerItem.insertBefore($newQuestionItem.find("tr.answerAnchor"));

                    }

                    return $newQuestionItem;


    //			[[WORDS QUESTION]]
                case "words":

                    $newQuestionItem = $($.parseHTML(localVars.htmlElements.words.questionItem));
                    return $newQuestionItem;


    //			[[OTHER QUESTION]]
                case "other":
                    break;
                default:
                    break;
            }

        },



        /**
         * GENERATES A JQUERY OBJECT THAT REPRESENTS A ANSWER ELEMENT
         * @param   {String}        type           [[The answer type requested. A dbValue string must be used. See cqCommonFuncs.js, localVars.questionTypes]]
         * @param   {Boolean}       withoutButtton [[If defined and true then the button 'removeAnswerButton' is removed]]
         * @param   {Object}        data           [[If defined, it is used to feed the element]]
         * @returns {Jquery Object}
         */
        createAnswerItem: function(type, withoutButtton, data) {

            var $newAnswerItem;

//            [[TYPED-CONTENT]]
            switch(type) {


    //			[[MULTIPLE-CHOICE QUESTION]]
                case "multiple_choice":

                    $newAnswerItem = $($.parseHTML(localVars.htmlElements.multiple_choice.answerItem));

                    if(withoutButtton) {
                        $newAnswerItem.find("button.removeAnswerButton").remove();
                    }

                    if(data) {
                        $newAnswerItem
                            .find("td.answerIndex")
                            .text(data.aLetter)
                        .end()
                            .find("[name=answerTitle]")
                            .val(data.aTitle)
                        .end()
                            .find("[name=answerCorrect]")
                            .val(data.aCorrect.toString());
                    }

                    return $newAnswerItem;


    //			[[WORDS QUESTION]]
                case "words":

                    $newAnswerItem = $($.parseHTML(localVars.htmlElements.words.answerItem));
                    return $newAnswerItem;


    //			[[OTHER QUESTION]]
                case "other":

                default:
                    break;

            }

        },


        /**
         * GENERATES THE QUESTION AND ANSWER ITEMS NEEDED AND
         * INSERT THEM IN THE DOM DOCUMENT
         * @param {Object}   quiz           [[The Quiz object used to feed the HTML elements]]
         * @param {JQuery Object} questionAnchor [[Jquery object that represents the place where the question items have to be inserted]]
         */
        generateFormFromData: function(quiz, questionAnchor) {

            _.forEach(quiz.questions, function(q, qKey) {

                var $newQuestionItem = CqQcFuncs.quiz.createQuestionItem(q.qType, q);


//                [[TYPED-CONTENT]]
                switch(q.qType) {


    //				[[MULTIPLE-CHOICE QUESTION]]
                    case "multiple_choice":

                        _.forEach(q.qAnswers, function(a, aKey){
                            var $newAnswerItem;
                            if(aKey === 0) {
                                $newAnswerItem = CqQcFuncs.quiz.createAnswerItem(q.qType, true, a);
                            } else {
                                $newAnswerItem = CqQcFuncs.quiz.createAnswerItem(q.qType, false, a);
                            }
                            $newAnswerItem.insertBefore($newQuestionItem.find("tr.answerAnchor"));
                        });

                        break;


    //				[[WORDS QUESTION]]
                    case "words":


    //					************
    //					TO IMPLEMENT
    //					************

                        break;


    //				[[OTHER QUESTION]]
                    case "other":
                        break;


    //				[[DEFAULT BEHAVIOUR]]
                    default:
                        break;
                };

                $newQuestionItem.insertBefore(questionAnchor);
                Session.set('questionsCount', Session.get('questionsCount') + 1);

            });

        }



    },

    session: {

        /**
         * SCANS THE SESSION FORM TO GENERATE SESSION OBJECT
         * @returns {Object} [[Object that contains a few of the session properties]]
         */
        scanSessionForm: function() {
            var newSession = {};
            newSession.quizId = $("#sessionQuiz").val();
            newSession.sessionCode = $("#quizCodePrefix").text() + $("[name=codeSuffix]").val();
            return newSession;
        },

        /**
         * GENERATES A CODESUFFIX STRING. USED IN THE SESSION CREATION FORM
         * @param   {Number} totalSessionsCreated [[Represents the totalSessionsCreated property of the selected Quiz]]
         * @returns {String} [[Represents the codeSuffix that will be used in the Session.sessionCode (Session.sessionCode = Quiz.codePrefix + codeSuffix). This way, it is certain the sessionCode is unique. Also, it is limited to 3 characters: 001 to 999]]
         */
        getCodeSuffixString: function(totalSessionsCreated) {
            var codeSuffixStr = "" + (totalSessionsCreated + 1);
            while(codeSuffixStr.length < 3) {
                codeSuffixStr = "0" + codeSuffixStr;
            };
            return codeSuffixStr;
        }

    }

}
