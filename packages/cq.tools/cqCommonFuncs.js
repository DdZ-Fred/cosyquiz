var localVars = {


	questionTypes: [
		{
			listName: "multiple-choice",
			questionItemClass: "questionMultipleChoice",
			answerItemClass: "answerMultipleChoice",
			dbValue: "multiple_choice"
			},
		{
			listName: "words",
			questionItemClass: "questionWords",
			answerItemClass: "answerWords",
			dbValue: "words"
			}
		]

};

CqCommonFuncs = {
	
	/**
	 * SIMPLY RETURNS THE ARRAY OF AVAILABLE TYPES (available above)
	 * @returns {Array}
	 */
	getQuestionTypes: function () {
		return localVars.questionTypes;
	},

	
	/**
	 * RETURNS THE QUESTION ITEM CLASS ASSOCIATED TO THE GIVEN 'dbValue'
	 * @param   {String} dbValue [[The dbValue (see localVars.questionTypes above) of a question type]]
	 * @returns {String} 
	 */
	getQuestionItemClass: function(dbValue) {
		_ = lodash;
		var index = _.findIndex(localVars.questionTypes, {dbValue: dbValue});
		return localVars.questionTypes[index].questionItemClass;
	},
	
	/**
	 * RETURNS THE DBVALUE ASSOCIATED TO THE GIVEN questionItemClass / REVERSE OF getQuestionItemClass()
	 * @param {String} questionItemClass [[The questionItemClass of a question type]]
	 * @returns {String} 
	 */
	getDbValue: function(questionItemClass) {
		_ = lodash;
		var index = _.findIndex(localVars.questionTypes, {questionItemClass: questionItemClass});
		return localVars.questionTypes[index].dbValue;
	}
};
