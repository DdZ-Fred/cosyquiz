Template.qcLeftMenu.helpers({
    getReady: function() {
        return {listingType: 'ready'};
    },
	getInProgress: function() {
		return {listingType: 'inProgress'}	
	},
    getPast: function() {
        return {listingType: 'past'};
    }
});
