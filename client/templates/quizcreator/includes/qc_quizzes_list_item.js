Template.qcQuizzesListItem.helpers({
    date: function () {
        return moment(this.createdAt).format("DD/MM/YYYY, HH:mm");
    }
});
