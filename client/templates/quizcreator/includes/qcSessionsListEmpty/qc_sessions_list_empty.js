Template.qcSessionsListEmpty.helpers({

	getTypedEmptyTemp: function () {

		switch (Template.parentData(1).listingType) {

			case 'ready':
				return 'qcSessionsListEmptyReady';
			case 'inProgress':
				return 'qcSessionsListEmptyInProgress';
			case 'past':
				return 'qcSessionsListEmptyPast';
			default:
				return 'sessionlistingTypeError';

		}

	},

});
