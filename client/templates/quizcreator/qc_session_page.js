/**
 * CALLED WHEN AN INSTANCE OF THE TEMPLATE IS RENDERED (CALLED ONCE)
 */
Template.qcSessionPage.onRendered(function() {

/*	var quizzes = Quizzes.find({}, {
        sort: {
            createdAt: -1
        }
    }).fetch();

    var $optionTemp = $("option.hidden");
    if (quizzes.length > 0) {

        _.forEach(quizzes, function (quiz, index) {
            $optionTemp.clone()
                .removeAttr("class")
                .attr("value", quiz._id)
                .text(quiz.title)
                .insertBefore($optionTemp);

            if (quiz._id === Template.currentData().session.quizId) {
                $("#quizCodePrefix").text(quiz.codePrefix);
                $("#quizSubtitle").val(quiz.subtitle);
                $("#sessionQuiz").val(quiz._id);
                var str1 = Template.currentData().session.sessionCode.replace(quiz.codePrefix, "");
                $("[name=sessionCode]").val(str1);
            }
        });

    }*/


});

Template.qcSessionPage.helpers({

    codeSuffix: function() {
        return this.session.sessionCode.replace(this.quiz.codePrefix, "");
    }
});



/**
 * TEMPLATE EVENT LISTENERS
 */
Template.qcSessionPage.events({

//	[[Delete session document]]
    'click .sessionDeleteButton': function(e) {

        e.preventDefault();

//		...Better to redirect first and then do the oprations of the DB to prevent exceptions, errors with the UI
        Router.go('qcSessionsList', {listingType: 'ready'});

        Meteor.call("sessionRemove", this.session._id, function(error, result) {
            if(error) console.log("METHOD ERROR: " + error.reason);
        });

    },


    'click .goToSessionBroadcastButton': function(e) {

        e.preventDefault();

        Router.go('qcSessionBroadcast', {_id: this.session._id});

    }

});
