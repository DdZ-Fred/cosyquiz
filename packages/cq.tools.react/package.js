Package.describe({

    name: "ddzfred:cq.tools.react",
    summary: "Gathers all the React components needed for my application CosyQuiz",
    version: "1.0.18"

});

Package.on_use(function(api) {

//	[[Dependencies: External packages needed on the client for a proper functionning]]
    api.use("react", "client");
    api.use("jquery", "client");
    api.use("iron:router@1.0.12", "client");
    api.use("ddzfred:cq.tools", "client");
    api.use("ddzfred:cq.tools.au-content-processing", "client");
    api.use("ddzfred:cq.tools.au-result-processing", "client");
    api.use("ddzfred:cq.tools.qc-content-processing", "client");
    api.use("ddzfred:cq.tools.qc-result-processing", "client");

//	[[Application level React components - Used anywhere]]
    api.add_files("common/Timer_React.jsx", "client");
    api.export("Timer", "client");

//	[[React components for the QuizCreator environment]]
    api.add_files("quizcreator/QuestionContentQC_React.jsx", "client");
    api.export("QuestionContentQC", "client");
    api.add_files("quizcreator/QuestionResultQC_React.jsx", "client");
    api.export("QuestionResultQC", "client");


//	[[React components for the Audience environment]]
    api.add_files("audience/QuestionContentAU_React.jsx", "client");
    api.export("QuestionContentAU", "client");
    api.add_files("audience/QuestionResultAU_React.jsx", "client");
    api.export("QuestionResultAU", "client");


//	[[React components for the SuperAdmin environment]]


});
