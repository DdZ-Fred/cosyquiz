Template.qcSessionBroadcastFinished.onRendered(function() {
    React.render(
        React.createElement(Timer, {
                                initialTime: 5,
                                type: 'redirection',
                                typeParams: {
                                    routeRedirected: 'qcSessionPage',
                                    routeParams: {_id: Template.currentData().sessionId},
                                    pageName: 'session results'
                                }
                            }
        ),
        document.getElementById('timerAnchor')
    );
});
