# Master's Project - A Wifi-based Mobile Quiz #

CosyQuiz is a real-time response (quiz) system. This later was fully developed in JavaScript using a wide range of tools among which can be found:

* Meteor (v 1.2) + MongoDB
* ReactJs + ES6
* Lodash
* MomentJs


The system gathers 3 environments, each one targeting a user role:

### QuizCreator: ###
The account creation needs the approval of a SuperAdmin.

The user can:

* CRUD Quizzes (a set of questions and answers).
* CRUD Sessions (instances of a Quiz, a same Quiz can be reused several times)
* launch a Session, which the Audience-type users can join.
* drives the Session through the successive Questions and their respective results/answers (from the whole audience)


### SuperAdmin: ###
* can see and approve QuizCreator account requests

### Audience ###
The account creation doesn't require a SuperAdmin's approval.

The user can:

* Join a Session and start answering the questions.
* See his own results to a question