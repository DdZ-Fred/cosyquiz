Template.saAccountReqItem.helpers({
    email: function() {
        return this.emails[0].address;
    }
});

Template.saAccountReqItem.events({
    'click .activate': function(e) {
        e.preventDefault();
        Meteor.call('activateAccount', this._id);
    }
});
