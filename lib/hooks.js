
/*************************
 ****** DEPENDENCIES *****
 *************************/

var CQ_Tools = {
    roleLayout: {
        superadmin: 'saLayout',
        quizcreator: 'qcLayout',
        audience: 'auLayout',
        notConnected: 'layout'
    },
    //    INFO: returns the Home route of the role put in parameter
    getRoleHome: function (role) {

        switch (role) {
            case 'superadmin':
                return 'saHome';
            case 'quizcreator':
                return 'qcHome';
            case 'audience':
                return 'auHome';
        }
    },
    getEnvironment: function () {

        var currentLayoutTemplate = this.layoutTemplate;

        /*                The string is by default searched from the end to the beginning.
                    The 2nd arg is the position where the search will start.*/
        if (currentLayoutTemplate.lastIndexOf('sa', 0) === 0) {
            return 'superadmin';
        } else if (currentLayoutTemplate.lastIndexOf('qc', 0) === 0) {
            return 'quizcreator';
        } else if (currentLayoutTemplate.lastIndexOf('au', 0) === 0) {
            return 'audience';
        } else {
            return 'random';
        }
    }
};


/*************************/
/*************************/
/***  1. BEFORE HOOKS  ***/
/*************************/
/*************************/


var CQ_BeforeHooks = {

    rootBeforeHook: function () {

        var user = Meteor.user();

        if (!user) {
            if (Meteor.loggingIn()) {
                this.render(this.loadingTemplate);
            } else {
                this.render('root');
            }
        } else {
            Router.go(CQ_Tools.getRoleHome(user.role));
        }
    }
};





/**********************************************/
/***  1.1 START - GLOBAL LEVEL APPLICATION  ***/
/**********************************************/





/*************************************/
/***  1.2 ROUTE LEVEL APPLICATION  ***/
/*************************************/


// ROOT
Router.onBeforeAction(CQ_BeforeHooks.rootBeforeHook, {
    only: ['root']
});


/********************************************/
/***  1.3 END - GLOBAL LEVEL APPLICATION  ***/
/********************************************/





/************************/
/************************/
/***  2. AFTER HOOKS  ***/
/************************/
/************************/

var CQ_AfterHooks = {

};

/**********************************************/
/***  2.1 START - GLOBAL LEVEL APPLICATION  ***/
/**********************************************/



/*************************************/
/***  2.2 ROUTE LEVEL APPLICATION  ***/
/*************************************/



/********************************************/
/***  2.3 END - GLOBAL LEVEL APPLICATION  ***/
/********************************************/





/**************************/
/**************************/
/***  3. ON-STOP HOOKS  ***/
/**************************/
/**************************/

Router.onStop(function () {
    delete Session.keys['questionsCount'];
}, {
    only: ['qcQuizCreate', 'qcQuizPage']
});

Router.onStop(function () {
    delete Session.keys['quizId'];
}, {
    only: 'qcQuizPage'
});
