ownsDocument = function (userId, doc) {

    return doc && doc.userId === userId;
};

isSuperAdmin = function () {
    var user = Meteor.user();
    return user && user.role === "superadmin";
};

isQuizCreator = function () {
    var user = Meteor.user();
    return user && user.role === "quizcreator";
};

isAudience = function () {
    var user = Meteor.user();
    return user && user.role === "audience";
};
