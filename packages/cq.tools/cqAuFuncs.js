CqAuFuncs = {

    scanQuestionAnswer: function (questionType) {

        var newParticipant = new CQ_Constructs.Participant(questionType);

        switch (questionType) {

            case "multiple_choice":

                $("form.auQuestionForm")
                    .find("label")
                    .each(function (cIndex, c) {
                        if ($(c).find("input.choice").is(":checked")) {
                            newParticipant.choices.push($(c).find("input.choice").val());
                        }
                    });

                //				[[CHECK IF CHOICES HAVE BEEN SELECTED]]
                if (newParticipant.choices.length > 0) {
                    return newParticipant;
                }

                break;

            case "words":
                break;
            default:
                break;
        }

    }

};
