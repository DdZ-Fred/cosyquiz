/*INFO: Routing config
- Routes(what to do when a path is requested)
- Templates to render
- Hooks(similar to Java filters e.g onBeforeAction...)*/

Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading',
    notFoundTemplate: "notFound",
    waitOn: function () {
        return Meteor.subscribe('userData');
    }

});

Router.route('/', {
    name: 'root'
});





/****************/
/**SUPER ADMIN***/
/****************/

Router.route('/sa/home', {
    name: 'saHome',
    controller: 'SA_Controller'
});

Router.route('/sa/accountrequests', {
    name: 'saAccountRequests',
    controller: 'SA_Controller'
});

Router.route('/sa/statistics', {
    name: 'saStatistics',
    controller: 'SA_Controller'
});

Router.route('/sa/settings', {
    name: 'saSettings',
    controller: 'SA_Controller'
});



/****************/
/**QUIZ CREATOR**/
/****************/

Router.route('/qc/home', {
    name: 'qcHome',
    controller: 'QC_Controller'
});

Router.route('/qc/quizcreate', {
    name: 'qcQuizCreate',
    controller: 'QC_Controller'
});

Router.route('/qc/quizzeslist/:quizzesLimit?', {
    name: 'qcQuizzesList',
    controller: 'QC_Controller'
});

Router.route('qc/quiz/:_id', {
    name: 'qcQuizPage',
    controller: 'QC_Controller',
    data: function () {
        return  {
            quiz: Quizzes.findOne({_id: this.params._id})
        };
    }
});

Router.route('qc/sessionCreate', {
    name: 'qcSessionCreate',
    controller: 'QC_Controller'
});

Router.route('qc/session/:_id', {
    name: 'qcSessionPage',
    controller: 'QC_Controller',
    data: function() {
        return {
            session: Sessions.findOne(),
            quiz: Quizzes.findOne(),
            sessionQuestionResults: SessionQuestionResults.find()
        };
    }
});

Router.route('qc/sessionsList/:listingType/:sessionsLimit?', {
    name: 'qcSessionsList',
    controller: 'QC_Controller',
    data: function () {

        tempData = {
            listingType: this.params.listingType
        };

        return tempData;
    }
});

Router.route('qc/sessionBroadcast/:_id', {
    name: 'qcSessionBroadcast',
    controller: 'QC_Controller',
    data: function () {
        data = {
            session: Sessions.findOne()
        };

        return data;

    }
});


/****************/
/****AUDIENCE****/
/****************/

Router.route('/au/home', {
    name: 'auHome',
    controller: 'AU_Controller'
});

Router.route('/au/sessionBroadcast/:_id', {
    name: 'auSessionBroadcast',
    controller: 'AU_Controller',
    data: function() {
        data = {
            session: Sessions.findOne()
        };

        return data;
    }
});



/****************/
/*****GLOBAL*****/
/****************/
