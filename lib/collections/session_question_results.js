/*****************************/
/*** COLLECTION DEFINITION ***/
/*****************************/


/**
 * The collection has been optimized specifically for the QuizCreator need
 * to show/see the results of a single question (Stats on the total amount of users...). It is where the Audience response
 * to a question is inserted. Due to the Meteor limitations and the wish for good performances,
 * another collection called UserSessionResults has been set up to allow getting a session results on
 * the Audience side.
 */
SessionQuestionResults = new Mongo.Collection("sessionQuestionResults");




/**********************/
/***  DEPENDENCIES  ***/
/**********************/





/*********************/
/***     RULES     ***/
/*********************/





/*********************/
/***    METHODS    ***/
/*********************/

Meteor.methods({

/*    This method is called at the quiz session launch.
    It allows to initialize the SessionQuestionResults docs (one per question) necessary
    for the audience to send their answers. */
    sessionQuestionResultsInsert: function(userId, sessionId, quizId, questionsInfo) {

        check(userId, String);
        check(sessionId, String);
        check(quizId, String);

        _ = lodash;

        _.forEach(questionsInfo, function(qInfoItem, key) {
            check(qInfoItem._id, String);
            check(qInfoItem.qType, String);
        });

        if(Meteor.isServer) {

            if(Sessions.findOne({_id: sessionId}) && Quizzes.findOne({_id: quizId})) {

/*                One SessionQuestionResults doc is inserted per question.
                All the ids are stored in this variable */
                var insertedSessionQuestionResultsIds = [];

                _.forEach(questionsInfo, function(qInfoItem, key) {

//                    SessionQuestionResults doc currently processed
                    var sessionQuestionResults =
                        new CQ_Constructs.SessionQuestionResults(sessionId, quizId, qInfoItem._id, qInfoItem.qType);
                    _.extend(sessionQuestionResults, {
                        userId: userId
                    });

//                    Current SessionQuestionResults id
                    var sessionQuestionResultsId = SessionQuestionResults.insert(sessionQuestionResults);
//                    The id returned is then added to the array created earlier
                    insertedSessionQuestionResultsIds.push(sessionQuestionResultsId);

                });

                return insertedSessionQuestionResultsIds;

            } else {
                throw new Meteor.Error("Invalid dependencies", "Session and/or Quiz docs cannot be found");
            }


        } else {
            throw new Meteor.Error("Invalid requester", "Not called by a QuizCreator or from the Server");
        }



    },

    /**
     * [[TYPED-BLOCK]]
     * UPDATES THE SQR DOC ASSOCIATED TO THE CURRENT SESSION WITH THE AUDIENCE USER'S ANSWER
     * @throws {Meteor.Error} [[Description]]
     * @param   {Object}   params         [[Description]]
     * @param   {Object}   newParticipant [[Description]]
     * @returns {[[Type]]} [[Description]]
     */
    sqrUpdateWithAudienceAnswer: function(params, newParticipant) {

        this.unblock();

        _ = lodash;

        check(this.userId, String);
        check(params, {
            sessionId: String,
            questionId: String,
            questionType: String
        });


//		CREATE A CUSTOM CHECK SO THAT THE RIGHT PROPERTIES, FOR EACH QUESTION TYPE, CAN BE CHECKED
        switch(params.questionType) {
            case 'multiple_choice':

                _.forEach(newParticipant.choices, function(choice, key) {
                    check(choice, String);
                });

                break;

            case 'words':
                break;
            default:
                break;
        }

        var user = Meteor.user();
        if(user && user.role === 'audience') {

            if(SessionQuestionResults.findOne({
                sessionId: params.sessionId,
                questionId: params.questionId})) {

                var finalParticipant = _.extend(newParticipant, {
                    userId: user._id
                });

                var sqrUpdate = SessionQuestionResults.update({
                    sessionId: params.sessionId,
                    questionId: params.questionId
                }, {
                    $push: {
                        "results.participants": finalParticipant
                    },
                    $inc: {
                        "results.participantsCount": 1
                    }
                });

                return sqrUpdate;

            }

        } else {
            throw new Meteor.Error("Invalid user", "Not logged or/and not an Audience user");
        }

    },


    sessionQuestionResultsRemove: function(sessionId) {

        check(sessionId, String);

        var user= Meteor.user();
        if(user && user.role === 'quizcreator') {

            var remove = SessionQuestionResults.remove({
                sessionId: sessionId,
                userId: this.userId
            });

            return remove;

        } else {
            throw new Meteor.Error("Invalid user", "Not logged or/and not an Audience user");
        }

    }



});
