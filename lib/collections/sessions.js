/*****************************/
/*** COLLECTION DEFINITION ***/
/*****************************/

Sessions = new Mongo.Collection("sessions");




/**********************/
/***  DEPENDENCIES  ***/
/**********************/





/*********************/
/***     RULES     ***/
/*********************/





/*********************/
/***    METHODS    ***/
/*********************/

Meteor.methods({

    /**
     * INSERTS IN THE DB A NEW SESSION DOC
     * @param   {Object} session [[Contains the _id of the Quiz doc associated and a newly generated sessionCode]]
     * @returns {Object} [[That contains the _id of the newly inserted Session doc together with its sessionCode]]
     */
    sessionInsert: function (session) {

        check(this.userId, String);
        check(session, {
            quizId: String,
            sessionCode: String
        });

        var user = Meteor.user();
        if (user && user.role === "quizcreator") {

            var finalSession = _.extend(session, {
                createdAt: new Date(),
                launchedAt: null,
                finishedAt: null,
                userId: this.userId,
                state: null
            });

            var sessionId = Sessions.insert(finalSession);

            if (Meteor.isServer) {


                Meteor.defer(function () {

                    _ = lodash;

                    //				[[1. Update the associated Quiz (incrementation of 2 properties: sessionsCount and totalSessionsCreated)]]
                    Meteor.call('quizUpdateForSessionCreation', finalSession.quizId, function (error, result) {
                        if (error) return console.log("sessionInsert: sub-method call quizUpdateForSessionCreation ERROR: " + error.reason);

                    });

                    //				[[2. Insertion of its SessionQuestionResults docs (one for each question)]]

                    //				[[2.1. Retrieving the Quiz questions' Ids and Types.]]
                    var quiz = Quizzes.findOne({
                        _id: finalSession.quizId
                    }, {
                        fields: {
                            questions: 1
                        }
                    });

                    //				[[2.2. Extraction of the data needed]]
                    var questionsInfo = [];
                    _.forEach(quiz.questions, function (q, key) {
                        questionsInfo.push({
                            _id: q._id,
                            qType: q.qType
                        });
                    });

                    //				[[2.3. Inserting the SessionQuestionResults docs using questionsInfo]]
                    Meteor.call('sessionQuestionResultsInsert',
                        user._id,
                        sessionId,
                        finalSession.quizId,
                        questionsInfo,
                        function (error, result) {
                            if (error) return console.log("sessionInsert: sub-method call sessionQuestionResultsInsert ERROR: " + error.reason);
                        });

                });
            }

            return {
                _id: sessionId,
                sessionCode: finalSession.sessionCode
            };

        } else {
            throw new Meteor.Error("Invalid user", "Not logged or/and not a QuizCreator");
        }

    },

    /**
     * RETURNS THE ID OF THE SESSION DOC IDENTIFIABLE BY THE sessioCode GIVEN
     * @param   {String} sessionCode [[sessionCode of the Session doc requested/needed]]
     * @returns {Object} [[Simple object containing a unique property, the _id of the Session doc requested/needed]]
     */
    sessionGetId: function (sessionCode) {

        check(sessionCode, String);

        var user = Meteor.user();

        if (user && (user.role === 'audience' || user.role === 'quizcreator')) {

            var sessionDoc = Sessions.findOne({
                sessionCode: sessionCode
            }, {
                fields: {
                    _id: 1
                }
            });

            return sessionDoc;

        } else {
            throw new Meteor.Error("Invalid user", "Not logged or/and not a QuizCreator/Audience")
        }
    },

    sessionUpdate: function (id, newSession) {

        check(id, String);
        check(session, {
            quizId: String,
            sessionCode: String
        });

        var user = Meteor.user();

        if (user && user.role === 'quizcreator') {

            var oldSession = Sessions.findOne({
                _id: id
            });

            if (oldSession) {

                var update = Sessions.update({
                    _id: id
                }, {
                    $set: {
                        quizId: newSession.quizId,
                        sessionCode: newSession.sessionCode
                    }
                });

                return update;

            } else {
                throw new Meteor.Error("Invalid session doc", "Document cannot be found");
            }

        } else {
            throw new Meteor.Error("Invalid user", "Not logged or/and not a QuizCreator");
        }

    },

    /**
     * REMOVES/DELETES THE SESSION DOCUMENT IN DB
     * @param   {String} id [[Id of the session doc to remove]]
     * @returns {Object} [[Result of the remove operation]]
     */
    sessionRemove: function (id) {

        check(id, String);

        var user = Meteor.user();

        if (user && user.role === 'quizcreator') {

            var session = Sessions.findOne({
                _id: id
            });

            var remove = Sessions.remove({
                _id: id
            });

            if (Meteor.isServer) {
                Meteor.call('quizUpdateForSessionDeletion', session.quizId, function (error1, result1) {
                    if (error1) return console.log("METHOD quizUpdateForSessionDeletion ERROR: " + error1.reason);
                });

                //				SessionQuestionResults docs are inserted at the Session launch...
                if (session.launchedAt) {
                    Meteor.call('sessionQuestionResultsRemove', id, function (error2, result2) {
                        if (error2) return console.log("METHOD sessionQuestionResultsRemove ERROR: " + error2.reason);
                    });
                }

            }


            return remove;
        } else {
            throw new Meteor.Error("Invalid user", "Not logged or/and not a QuizCreator");
        }

    },

    sessionLaunchUpdate: function (id) {

        check(this.userId, String);
        check(id, String);

        var user = Meteor.user();
        if (user && user.role === "quizcreator") {

            var session = Sessions.find({
                _id: id
            });
            if (session) {
                var update = Sessions.update({
                    _id: id
                }, {
                    $set: {
                        launchedAt: new Date(),
                        state: {
                            type: 'q',
                            index: 0
                        }
                    }
                });
                return update;

            } else {
                throw new Meteor.Error("Invalid session doc", "Document cannot be found");
            }


        } else {
            throw new Meteor.Error("Invalid user", "Not logged or/and not a QuizCreator");
        }

    },

    sessionGoToQuestionResultsUpdate: function (id) {

        check(this.userId, String);
        check(id, String);

        var user = Meteor.user();

        if (user && user.role === 'quizcreator') {

            var session = Sessions.find({
                _id: id
            });
            if (session) {
                var update = Sessions.update({
                    _id: id
                }, {
                    $set: {
                        "state.type": 'r'

                    }
                });
                return update;

            } else {
                throw new Meteor.Error("Invalid session doc", "Document cannot be found");
            }



        } else {
            throw new Meteor.Error("Invalid user", "Not logged or/and not a QuizCreator");
        }

    },

    sessionGoToNextQuestionUpdate: function (id) {

        check(this.userId, String);
        check(id, String);

        var user = Meteor.user();

        if (user && user.role === 'quizcreator') {

            var session = Sessions.find({
                _id: id
            });
            if (session) {

                var update = Sessions.update({
                    _id: id
                }, {
                    $set: {
                        "state.type": 'q'

                    },
                    $inc: {
                        "state.index": 1
                    }
                });
                return update;

            } else {
                throw new Meteor.Error("Invalid session doc", "Document cannot be found");
            }

        } else {
            throw new Meteor.Error("Invalid user", "Not logged or/and not a QuizCreator");
        }

    },

    sessionReturnToQuestionContent: function (id) {

        check(this.userId, String);
        check(id, String);

        var user = Meteor.user();

        if (user && user.role === 'quizcreator') {

            var session = Sessions.find({
                _id: id
            });

            if (session) {

                var update = Sessions.update({
                    _id: id
                }, {
                    $set: {
                        "state.type": 'q'
                    }
                });

                return update;

            } else {
                throw new Meteor.Error("Invalid session doc", "Document cannot be found");
            }



        } else {
            throw new Meteor.Error("Invalid user", "Not logged and/or not a QuizCreator");
        }

    },

    sessionReturnToPreviousQuestionResult: function (id) {

        check(this.userId, String);
        check(id, String);

        var user = Meteor.user();

        if (user && user.role) {

            var session = Sessions.find({
                _id: id
            });

            if (session) {

                var update = Sessions.update({
                    _id: id
                }, {
                    $set: {
                        "state.type": 'r'
                    },
                    $inc: {
                        "state.index": -1
                    }
                });

                return update;

            } else {
                throw new Meteor.Error("Invalid session doc", "Document cannot be found");
            }

        } else {
            throw new Meteor.Error("Invalid user", "Not logged and/or not a QuizCreator");
        }


    },

    sessionGoToEndUpdate: function (id) {


        check(this.userId, String);
        check(id, String);

        var user = Meteor.user();

        if (user && user.role === 'quizcreator') {

            var session = Sessions.find({
                _id: id
            });
            if (session) {

                var update = Sessions.update({
                    _id: id
                }, {
                    $set: {
                        state: null
                    }
                });
                return update;

            } else {
                throw new Meteor.Error("Invalid session doc", "Document cannot be found");
            }


        } else {
            throw new Meteor.Error("Invalid user", "Not logged or/and not a QuizCreator");
        }


    },

    sessionReturnToLastQuestionResults: function (id, count) {

        check(this.userId, String);
        check(id, String);
        check(count, Number);

        var user = Meteor.user();

        if (user && user.role) {

            var session = Sessions.find({
                _id: id
            });

            if (session) {

                var update = Sessions.update({
                    _id: id
                }, {
                    $set: {
                        state: {
                            type: 'r',
                            index: count - 1
                        }
                    }
                });

                return update;

            } else {
                throw new Meteor.Error("Invalid session doc", "Document cannot be found");
            }

        } else {
            throw new Meteor.Error("Invalid user", "Not logged or/and not a QuizCreator");
        }

    },

    sessionFinishUpdate: function (id) {

        check(this.userId, String);
        check(id, String);

        var user = Meteor.user();

        if (user && user.role === 'quizcreator') {

            var session = Sessions.find({
                _id: id
            });

            if (session) {
                var update = Sessions.update({
                    _id: id
                }, {
                    $set: {
                        finishedAt: new Date()
                    }
                });
            } else {
                throw new Meteor.Error("Invalid session doc", "Document cannot be found");
            }

        } else {
            throw new Meteor.Error("Invalid user", "Not logged or/and not a QuizCreator");
        }

    }
});
