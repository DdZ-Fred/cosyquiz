Template.qcSessionsList.helpers({


	emptySessionsList: function () {
		return Sessions.find().count() === 0;
	},

	/*Returns a String according the type of listing applied.
	The string is used in the Panel heading  */
	headingSecondPart: function () {

		switch (this.listingType) {

			case 'ready':
				return "Ready to launch";
			case 'inProgress':
				return "In progress";
			case 'past':
				return "Finished";
			default:
				return "OtherListingError";
		}

	},

	sessions: function () {

		switch (this.listingType) {

			case 'ready':
				
			return Sessions.find({}, {
				sort: {
					createdAt: -1
				}
			});
			case 'inProgress':
				return Sessions.find({}, {
					sort: {
						launchedAt: -1
					}
				});
				
			case 'past':
				return Sessions.find({}, {
					sort: {
						finishedAt: -1
					}
				})
		}
	}
});
