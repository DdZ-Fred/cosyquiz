Package.describe({
    name: "ddzfred:cq.tools.qc-result-processing",
    summary: "Gathers the logic that allows to process a question results of the whole audience" +
            " and arrange them appropriately for the QuizCreator-User need.",
    version: "1.0.6",
    documentation: "README.md"
});

Package.onUse(function(api) {

//    [[Dependencies]]
    api.use("jsx", "client");
    api.use("twbs:bootstrap@3.3.5", "client");
    api.use("stevezhu:lodash@3.10.1", "client");
    api.use("random", "client");
//    api.use("d3js:d3@3.5.5", "client");

//    [[Package files and exports]]
    api.add_files("qcResProc_MultipleChoice.jsx", "client");
    api.export("QcResProc_MultipleChoice", "client");

});
