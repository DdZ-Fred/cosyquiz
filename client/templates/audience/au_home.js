Template.auHome.events({
	
	
    'submit .startSessionForm': function(e) {

        e.preventDefault();

        var sessionCode = $(e.target).find("[name=sessionCode]").val();		
		
/*        Check existence of the session doc:
		- If exists, find Session doc ID and redirect to the Session broadcast
		- If not, show notification! (SHOW NOTIFICATION) */
		
		Meteor.call('sessionGetId', sessionCode, function(error, result) {
			
			if(error) return console.log("METHOD sessionGetId ERROR: " + error.reason);
			
			
			if(result) {
				Router.go('auSessionBroadcast', {_id: result._id});
			} else {
//				SHOW NOTIFICATION
				alert("This sessionCode cannot be found, please try again!");
			}
			
		});

    }
});
