Package.describe({
    name: "ddzfred:cq.tools.au-content-processing",
    summary: "Gathers the logic that allows to process a question content's appropriately",
    version: "1.0.4",
    documentation: "README.md"
});

Package.onUse(function(api) {

//    [[Dependencies]]
    api.use("jsx", "client");
    api.use("twbs:bootstrap@3.3.5", "client");

//    [[Packages files and exports]]
    api.add_files("auConProc_MultipleChoice.jsx", "client");
    api.export("AuConProc_MultipleChoice", "client");

});
