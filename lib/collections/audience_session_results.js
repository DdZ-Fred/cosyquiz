AudienceSessionResults = new Mongo.Collection("audienceSessionResults");

Meteor.methods({

    /**
     * INSERTS A NEW ASR DOC
     * @param   {String} sessionId [[_id of the session doc associated]]
     * @param   {String} userId    [[_id of the current logged user]]
     * @returns {Object} [[Contains the _id of the newly inserted asr doc]]
     */
    asrInsert: function (sessionId, userId) {

        check(sessionId, String);
        check(userId, String);

        var user = Meteor.user();

        if ((user && user.role === 'audience') || Meteor.isServer) {

            //			Stores the question items (and whether or not they have been answered)
            var questions = [];

            //			Returns an array of objects, each one of them containing the ID of a question from the current quiz session
            var sqrs = SessionQuestionResults.find({
                sessionId: sessionId
            }, {
                fields: {
                    sessionId: 1,
                    questionId: 1
                }
            }).fetch();

            //			We loop on the sqrs array to get just what we need
            _.forEach(sqrs, function (sqr, key) {
                questions.push({
                    questionId: sqr.questionId,
                    answered: false
                });
            });

            var asrId = AudienceSessionResults.insert({
                sessionId: sessionId,
                userId: userId,
                questions: questions
            });

            return {
                _id: asrId
            };

        } else {
            throw new Meteor.Error("Invalid environment", "Not logged as an Audience-User OR not called from Server");
        }
    },




    /**
     * [[TYPED-BLOCK (contains dynamic areas that depend on the question type)]]
     * UPDATES THE ASR DOC ASSOCIATED TO THE CURRENT AUDIENCE USER
     * @param {Object} params [[Contains the sessionId, questionId and questionType]]
     * @param {Object} answer [[Contains the Audience-type user's answer]]
     */
    asrUpdateWithQuestionAnswer: function (params, answer) {

        _ = lodash;

        check(this.userId, String);
        check(params, {
            sessionId: String,
            questionId: String,
            questionType: String,
            questionIndex: Number
        });

        //		CREATE A CUSTOM CHECK SO THAT THE RIGHT PROPERTIES, FOR EACH QUESTION TYPE, CAN BE CHECKED
        switch (params.questionType) {
            case 'multiple_choice':

                _.forEach(answer.choices, function (choice, key) {
                    check(choice, String);
                });

                break;

            case 'words':
                break;

            default:
                break;
        }

        var user = Meteor.user();
        if (user && user.role === 'audience') {

            if (AudienceSessionResults.findOne({
                    sessionId: params.sessionId,
                    userId: user._id
                })) {

                var update;

                switch (params.questionType) {
                    case 'multiple_choice':

                        update = AudienceSessionResults.update({
                            sessionId: params.sessionId,
                            userId: user._id,
                            "questions.questionId": params.questionId
                        }, {
                            $set: {
                                "questions.$.answered": true,
                                "questions.$.choices": answer.choices
                            }
                        });

                        break;

                    case 'words':
                        break;

                    default:
                        break;
                }

                return update;


            } else {
                throw new Meteor.Error("Invalid doc", "The current user's ASR doc couldn't be found");
            }


        } else {
            throw new Meteor.Error("Invalid user", "Not logged or/and not an Audience-type user");
        }



    }

});
