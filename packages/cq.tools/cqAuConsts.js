/****************
 * ** GLOSSARY **
 * **************
 * 
 * - au/Au/AU 	--> Audience
 * - qc/Qc/QC 	--> QuizCreator
 * - SB			--> SessionBroadcast
 * 
 * ***************/



/****************************************************************
 * GATHERS CONSTANTS THAT CAN BE USED IN THE AUDIENCE ENVIRONMENT
 * **************************************************************/
CqAuConsts = {

/*	The auSessionBroadcast page is a 'Micro Single Page Application' and showa multiple contents programatically.
	These multiple contents need multiple and different types of data. These constants are used to store and recognize
	the data that is needed. See /client/templates/audience/au_session_broadcast.js */
	SBdataType: {
		SESSION_ONLY: "S",
		SESSION_AND_QUIZ: "S_Q",
		SESSION_QUIZ_AND_ASR: "S_Q_ASR"
	}
	
};