var desAcc = Meteor.users.find({
   activated: false
});

Template.saAccountRequests.helpers({
    desactivatedAccounts: function () {
        return desAcc;
    },

    showRequests: function() {
        if(desAcc.count()) {
            return true;
        } else {
            return false;
        }
    }
});
