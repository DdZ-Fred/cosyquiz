Package.describe({

    summary: "Provides tools such as object constructors, reusable functions and objects for my application CosyQuiz",

    version: "1.0.65",

    name: "ddzfred:cq.tools"

});

Package.on_use(function (api) {

//  [[Dependencies / External packages]]
    api.use("random", "client");
    api.use("stevezhu:lodash", "client");



/*  ********************* */
/*	***  OWNED FILES  *** */
/*  ********************* */

//	[[COMMON]]
    api.add_files("cqConstructors.js", ["client", "server"]);
    api.export("CQ_Constructs", ["client", "server"]);

    api.add_files("cqCommonFuncs.js", "client");
    api.export("CqCommonFuncs", "client");

    api.add_files("cqCommonConsts.js", "client");
    api.export("CqCommonConsts", "client");


//	[[QUIZCREATOR]]
    api.add_files("cqQcFuncs.js", "client");
    api.export("CqQcFuncs", "client");

    api.add_files("cqQcConsts.js", "client");
    api.export("CqQcConsts", "client");


//	[[AUDIENCE]]
    api.add_files("cqAuFuncs.js","client");
    api.export("CqAuFuncs", "client");

    api.add_files("cqAuConsts.js", "client");
    api.export("CqAuConsts", "client");

});
