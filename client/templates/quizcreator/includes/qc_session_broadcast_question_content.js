Template.qcSessionBroadcastQuestionContent.onRendered(function() {
    React.render(
        React.createElement(QuestionContentQC, {
            question: Template.currentData().question
        }),
        document.getElementById('questionContentAnchor')
    );
});
