Template.qcSessionBroadcastCommands.helpers({
    quizEnd: function () {

/*        If the session has started and that the state is empty/null
        then the session is finished. And we should show the finish button.*/
        if(this.launchedAt && !this.finishedAt && !this.state) {
            return true;
        } else {
            return false;
        }
    }
});
