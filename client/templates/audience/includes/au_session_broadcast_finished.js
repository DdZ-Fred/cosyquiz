Template.auSessionBroadcastFinished.onRendered(function() {
    React.render(
        React.createElement(Timer, {
                                initialTime: 5,
                                type: 'redirection',
                                typeParams: {
                                    routeRedirected: 'auHome',
                                    routeParams: {},
                                    pageName: 'Home'
                                }
                            }
        ),
        document.getElementById('timerAnchor')
    );
});
