QuestionResultAU = React.createClass({

    mixins: [ReactMeteorData, AuResProc_MultipleChoice],

    getMeteorData() {
        return {
            asr: AudienceSessionResults.findOne()
        }
    },

    propTypes: {
        question: React.PropTypes.object.isRequired,
        qIndex: React.PropTypes.number.isRequired
    },

    isAnswered() {
        return this.data.asr.questions[this.props.qIndex].answered;
    },

    renderAnswerMessage() {
        if(this.isAnswered()) {
            return  <p>Your answer</p>;
        } else {
            return <p>No answer received</p>;
        }
    },
/**
 * TYPED-CONTENT
 * Renders the appropriate HTML content depending on the question type.
 * @returns {[[HTML]]} [[The appropriate HTML BLock]]
 */

    renderTypedResult() {

        switch(this.props.question.qType) {


/*            [[MULTIPLE-CHOICE]]*/
            case 'multiple_choice':

                return this.props.question.qAnswers.map((choice, cIndex)=> {

                    return  <div className="form-group" key={choice._id}>
                                {this.auResMcCheckboxNCorrectness(this.isAnswered(),
                                                                  choice,
                                                                  this.data.asr.questions[this.props.qIndex].choices)}
                            </div>
                    });


/*            [[WORDS]]*/
            case 'words':
                return <p>This is your Words question Result</p>;

            default:

            break;

        }

    },

    render() {

        return (
            <div className="panel panel-default">
                <p className="text-center">{this.props.question.qTitle}</p>
                <br />
                <br />
                <form className="auQuestionResult">
                    <fieldset disabled>
                        {this.renderAnswerMessage()}
                        <br />
                        {this.renderTypedResult()}
                    </fieldset>
                </form>
            </div>
        )
    }

});
