/**********************************************************************************************
 * DESCRIPTION: This file gathers the logic needed to process an answer for a question of type:
 * Multiple-Choice (Mc/mc/MC)
 * auRes = AudienceResult
 **********************************************************************************************/


AuResProc_MultipleChoice = {

    /**
     * COMPARES THE AUDIENCE-USER'S ANSWER WITH A POSSIBLE CHOICE TO A QUESTION TO RENDER THE APPROPRIATE HTML CONTENT
     * @param   {[[Boolean]]}   isAnswered [[Whether or not the question has been answered]]
     * @param   {Object}        choice     [[Possible choice currently processed/checked]]
     * @param   {[[Array]]}     auChoices  [[Choices selected by the Audience (au) user]]
     * @returns {[[HTML Node]]}    [[That contains the appropriate content]]
     */
    auResMcCheckboxNCorrectness(isAnswered, choice, auChoices) {

        var defaultCheckbox = <input className="choice" type="checkbox"/>;
        var checkboxToRender;

        var rightSign =     <span className="label label-success">
                                Good
                            </span>;
        var wrongSign =     <span className="label label-danger">
                                Wrong
                            </span>;
        var signToRender;

        if(isAnswered) {

/*            [[CHOICE WAS NOT SELECTED]]*/
            if(auChoices.indexOf(choice.aLetter) === -1) {

                checkboxToRender = defaultCheckbox;

/*                [[Check correctness]] */
                if(choice.aCorrect === true) {
                    signToRender = wrongSign;
                } else {
                    signToRender = rightSign;
                }


/*            [[CHOICE WAS SELECTED]]*/
            } else {

                checkboxToRender = <input className="choice" type="checkbox" readOnly checked />;


/*                [[Check correctness]] */
                if(choice.aCorrect === true) {
                    signToRender = rightSign;
                } else {
                    signToRender = wrongSign;
                }

            }

        } else {
            checkboxToRender = defaultCheckbox;
        }

        return  <label>
                    {choice.aTitle}
                    {checkboxToRender}
                    {signToRender}
                </label>

    }

}
