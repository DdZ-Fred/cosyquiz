Template.qcSessionBroadcastQuestionResults.onRendered(function() {
    React.render(
        React.createElement(QuestionResultQC, {
            question: Template.currentData().question
        }),
        document.getElementById('questionResultsAnchor')
    );
});
