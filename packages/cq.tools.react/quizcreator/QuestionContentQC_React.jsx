
QuestionContentQC = React.createClass({

    mixins: [QcConProc_MultipleChoice],

    propTypes: {
        question: React.PropTypes.object.isRequired
    },

    renderTypedContent() {

        switch(this.props.question.qType) {

            case 'multiple_choice':

                return this.qcConMc(this.props.question);
                break;
            case 'other':
                break;
            default:
                break;

        }

    },

    render() {

        return (
            <div className="panel panel-default">
                <p className="text-center">{this.props.question.qTitle}</p>
                <br />
                <br />
                {this.renderTypedContent()}
            </div>
        );

    }
});
