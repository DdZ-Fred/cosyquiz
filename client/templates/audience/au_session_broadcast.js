Template.auSessionBroadcast.onCreated(function () {

    var instance = this;

    instance.SBcontentType = new ReactiveVar("");
    instance.SBdataType = new ReactiveVar("");


    instance.autorun(function () {


        var localSession = Template.currentData().session;

        //		[[1. If session hasn't been launch yet]]
        if (!localSession.launchedAt && !localSession.state) {

            if (instance.SBcontentType.get() !== CqCommonConsts.SBcontentType.WELCOME) {
                instance.SBcontentType.set(CqCommonConsts.SBcontentType.WELCOME);
            }
            if (instance.SBdataType.get() !== CqAuConsts.SBdataType.SESSION_AND_QUIZ) {
                instance.SBdataType.set(CqAuConsts.SBdataType.SESSION_AND_QUIZ);
            }

            



            //		[[2. If session has been launched / is in progress]]
        } else if (localSession.launchedAt && localSession.state) {

            //			[[2.1. Content of type Question is needed]]
            if (localSession.state.type === 'q') {

                if (instance.SBcontentType.get() !== CqCommonConsts.SBcontentType.QUESTION) {
                    instance.SBcontentType.set(CqCommonConsts.SBcontentType.QUESTION);
                }
                if (instance.SBdataType.get() !== CqAuConsts.SBdataType.SESSION_QUIZ_AND_ASR) {
                    instance.SBdataType.set(CqAuConsts.SBdataType.SESSION_QUIZ_AND_ASR);
                }


                //			[[2.2. Content of type Result is needed]]
            } else if (localSession.state.type === 'r') {

                if (instance.SBcontentType.get() !== CqCommonConsts.SBcontentType.RESULT) {
                    instance.SBcontentType.set(CqCommonConsts.SBcontentType.RESULT);
                }
                if (instance.SBdataType.get() !== CqAuConsts.SBdataType.SESSION_QUIZ_AND_ASR) {
                    instance.SBdataType.set(CqAuConsts.SBdataType.SESSION_QUIZ_AND_ASR);
                }


            }

            //		[[3. If the session reaches its end]]
        } else if (localSession.launchedAt && !localSession.state) {

            //			[[3.1. Content of type End (End page showing thanks) is needed]]
            if (!localSession.finishedAt) {

                if (instance.SBcontentType.get() !== CqCommonConsts.SBcontentType.END) {
                    instance.SBcontentType.set(CqCommonConsts.SBcontentType.END);
                }
                if (instance.SBdataType.get() !== CqAuConsts.SBdataType.SESSION_QUIZ_AND_ASR) {
                    instance.SBdataType.set(CqAuConsts.SBdataType.SESSION_QUIZ_AND_ASR);
                }



                //			[[3.2. Content of type Finished is needed (page that shows a timer and redirects the user to the results)]]
            } else {
                if (instance.SBcontentType.get() !== CqCommonConsts.SBcontentType.FINISHED) {
                    instance.SBcontentType.set(CqCommonConsts.SBcontentType.FINISHED);
                }
                if (instance.SBdataType.get() !== CqAuConsts.SBdataType.SESSION_ONLY) {
                    instance.SBdataType.set(CqAuConsts.SBdataType.SESSION_ONLY);
                }


            }

        }

        var subscription = instance.subscribe('auSessionBroadcastDynaPub',
                                              localSession._id,
                                              instance.SBdataType.get());

    });

});


Template.auSessionBroadcast.onRendered(function () {


    var instance = this;

    //    Dynamic template region = auCurrentContent
    instance.autorun(function () {

//        [[Reactive var that triggers the Re-run of the Autorun block]]
        var sbContentType = instance.SBcontentType.get();

        
        if (instance.subscriptionsReady()) {

/*            The possible values have to reflect the ones available inside
            /packages/ddzfred:cq.tools/cqCommonConsts.js. See object CqCommonConsts. */
            switch (sbContentType) {

                //			[[CONTENT TYPE: WELCOME]]
                case "w":

                    Router.current().render('commonSessionBroadcastWelcome', {
                        to: 'auCurrentContent',
                        data: {
                            title: Quizzes.findOne().title,
                            subtitle: Quizzes.findOne().subtitle,
                            author: Quizzes.findOne().author
                        }
                    });

                    break;

                    //			[[CONTENT TYPE: QUESTION]]
                case "q":

                    Router.current().render('auSessionBroadcastQuestionForm', {
                        to: 'auCurrentContent',
                        data: {
                            title: Quizzes.findOne().title,
                            subtitle: Quizzes.findOne().subtitle,
                            qNumber: Template.currentData().session.state.index + 1,
                            qCount: Quizzes.findOne().questions.length,
                            question: Quizzes.findOne().questions[Template.currentData().session.state.index],
                            sessionId: Template.currentData().session._id
                        }
                    });

                    break;

                    //			[[CONTENT TYPE: RESULT]]
                case "r":

                    Router.current().render('auSessionBroadcastQuestionResult', {
                        to: 'auCurrentContent',
                        data: {
                            title: Quizzes.findOne().title,
                            subtitle: Quizzes.findOne().subtitle,
                            qNumber: Template.currentData().session.state.index + 1,
                            qCount: Quizzes.findOne().questions.length,
                            question: Quizzes.findOne().questions[Template.currentData().session.state.index]
                        }
                    });

                    break;

                    //			[[CONTENT TYPE: END]]
                case "e":

                    Router.current().render('auSessionBroadcastEnd', {
                        to: 'auCurrentContent'
                    });

                    break;

                    //			[[CONTENT TYPE: FINISHED]]
                case "f":

                    Router.current().render('auSessionBroadcastFinished', {
                        to: 'auCurrentContent'
                    });

                    break;


                default:
                    break;

            }
        } else {
            Router.current().render('commonSessionBroadcastLoadingData', {
                to: 'auCurrentContent'
            });
        }


    });


});


Template.auSessionBroadcast.helpers({

});
