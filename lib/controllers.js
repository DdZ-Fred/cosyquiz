/************************
 ******   1.TOOLS   *****
 ************************/

var CQ_Tools = {
    roleLayout: {
        superadmin: 'saLayout',
        quizcreator: 'qcLayout',
        audience: 'auLayout',
        notConnected: 'layout'
    },
    //    INFO: returns the Home route of the role put in parameter
    getRoleHome: function (role) {

        switch (role) {
            case 'superadmin':
                return 'saHome';
            case 'quizcreator':
                return 'qcHome';
            case 'audience':
                return 'auHome';
        }
    },
    getEnvironment: function () {

        var requestedRoute = Router.current().route.getName();

        /*                The string is by default searched from the end to the beginning.
                    The 2nd arg is the position where the search will start.*/
        if (requestedRoute.lastIndexOf('sa', 0) === 0) {
            return 'superadmin';
        } else if (requestedRoute.lastIndexOf('qc', 0) === 0) {
            return 'quizcreator';
        } else if (requestedRoute.lastIndexOf('au', 0) === 0) {
            return 'audience';
        } else {
            return 'random';
        }
    }
};





/************************************
 **   2. CONTROLLER BEFORE-HOOKS   **
 ************************************/

var CQ_ControllerBeforeHooks = {

    checkUserRights: function () {

        var user = Meteor.user();

        if (!user) {
            if (Meteor.loggingIn()) {
                this.render(this.loadingTemplate);
            } else {
                this.layout('layout');
                this.render('notFound');
            }
        } else {
            if (user.role === CQ_Tools.getEnvironment()) {
                this.next();
            } else {
                this.layout(CQ_Tools.roleLayout[user.role]);

                this.render('notFound');
            }
        }
    }

};





/***********************************
 **   3. CONTROLLERS DEFINITION   **
 ***********************************/


/**************/
/***  MAIN  ***/
/**************/
Main_Controller = RouteController.extend({
    onBeforeAction: CQ_ControllerBeforeHooks.checkUserRights
});



/********************/
/**  SUPER ADMIN  ***/
/********************/
SA_Controller = Main_Controller.extend({
    layoutTemplate: 'saLayout',
    waitOn: function() {
        return Meteor.subscribe('desactivatedAccountsCount');
    },
    yieldRegions: {
        'saLeftMenu': {
            to: 'saLeftMenu',
            data: function() {
                return {
                    desAccountsCount: Counts.get('desactivatedAccounts-count')
                }
            }
        }
    }


/*    data: function() {
        return {
            desAccountsCount: Counts.get('desactivatedAccounts-count')
        }
    }*/

});



/****************/
/**QUIZ CREATOR**/
/****************/
QC_Controller = Main_Controller.extend({
    layoutTemplate: 'qcLayout',
    yieldRegions: {
        'qcLeftMenu': {to: 'qcLeftMenu'}
    }

});



/****************/
/****AUDIENCE****/
/****************/
AU_Controller = Main_Controller.extend({
    layoutTemplate: 'auLayout'
});
