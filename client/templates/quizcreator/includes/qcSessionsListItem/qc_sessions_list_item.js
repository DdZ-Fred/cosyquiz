Template.qcSessionsListItem.helpers({

	getTypedSessionsListItem: function() {
		
		switch(Template.parentData(1).listingType) {
			case 'ready':
				return "qcSessionsListItemReady";
			case 'inProgress':
				return "qcSessionsListItemInProgress";
			case 'past':
				return "qcSessionsListItemPast";
			default:
				return "";
		}
		
	}

});
