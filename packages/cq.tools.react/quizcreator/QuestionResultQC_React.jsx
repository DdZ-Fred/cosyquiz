QuestionResultQC = React.createClass({

    mixins: [ReactMeteorData, QcResProc_MultipleChoice],

    getMeteorData() {
        return {
            sqr: SessionQuestionResults.findOne({
                    questionId: this.props.question._id
                })
        }
    },

    propTypes: {
        question: React.PropTypes.object.isRequired
    },

    renderTypedResult() {

        switch(this.props.question.qType) {
            case 'multiple_choice':

                return this.qcResMc(this.props.question.qAnswers, this.data.sqr.results);
                break;
            case 'words':
                break;
            default:
                break;
        }
    },

    render() {

        return (

            <div className="panel panel-default">
                <p className="text-center">{this.props.question.qTitle}</p>
                <br />
                <br />
                {this.renderTypedResult()}
            </div>
        )
    }

});
