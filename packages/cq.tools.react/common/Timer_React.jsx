/**
* REACT COMPONENT 'TIMER': RENDERS A TIMER OF A USER-DEFINED DURATION.
* THE COMPONENT TAKES AS A PROPERTY A TYPE TO ALLOW DIFFERENT BEHAVIOURS
* WHEN THE TIME EXPIRES.
* @param   {Number} initialTime    [[Represents the duration is seconds]]
* @param   {String} type           [[Allows different behaviours and messages. One of the values visible below]]
* @param   {Object} typeParams     [[Data that goes with the type specified]]
* @returns {HTML}   [[Represents the component rendered in HTML. See the 'render' function]]
*
* typeParams POSSIBLE VALUES
* --------------------------
*
* IF type='redirection':
*     typeParams= {
*         routeRedirected: {String},
*         routeParams: {Object},
*         pageName: {String}
*     }
*
* IF type='other':
*     typeParams= {
*         attributeOne: {String},
*         attributeTwo: {String}
*     }
*
**/

Timer = React.createClass({

    mixins: [],

    /**
    * [[Props: Similar to the parameters passed to a constructor to instantiate a class]]
     * See:  /client/templates/quizcreator/includes/qc_session_broadcast_finished.js
     */
    propTypes: {
        initialTime: React.PropTypes.number.isRequired,
        type: React.PropTypes.oneOf([
            'redirection',
            'other'
        ]).isRequired,
        typeParams: React.PropTypes.object.isRequired
    },


    /**
     * Allows to initialize the component
     * The state of a component is similar to the properties of a class we can find in OOP but for an HTML component.
     * Unlike the Prop, a state is mutable and reactive (the component is re-rendered when it is updated)]]
     * @returns {Object} [[Used as the initial value of this.state]]
     */
    getInitialState() {
        return {
            time: this.props.initialTime
        }
    },


    /**
    * Called before the component is mounted/inserted in the DOM doc.
    * Block of code taken from https://facebook.github.io/react/docs/reusable-components.html#mixins
    * and adapted for the need (Facebook Inc n.d.). 
     */
    componentWillMount() {
        this.intervals = [];
    },


    /**
    * Starts a new interval and pushes its ID in the array.
    * Block of code taken from https://facebook.github.io/react/docs/reusable-components.html#mixins
    * and adapted for the need (Facebook Inc n.d.)
     */
    setInterval() {
        this.intervals.push(setInterval.apply(null, arguments));
    },


    /**
    * Passes the function clearInterval to each one of the array items(every intervals) to clean/destroy them.
    * Block of code taken from https://facebook.github.io/react/docs/reusable-components.html#mixins
    * and adapted for the need (Facebook Inc n.d.).
     */
    componentWillUnmount() {
        this.intervals.map(clearInterval);
    },


    /**
    * Called immediately after the component is mounted/inserted in the DOM doc.
    * Block of code taken from https://facebook.github.io/react/docs/reusable-components.html#mixins
    * and adapted for the need (Facebook Inc n.d.).
     */
    componentDidMount() {
        this.setInterval(this.decrementTime, 1000);
    },


    /**
     * Decrements the value the state variable 'time' by 1.
     * Also, if the type specified in the component is 'redirection', it is here that
     * the redirection occurs when the time expires.
     * Block of code taken from https://facebook.github.io/react/docs/reusable-components.html#mixins
     * and adapted for the need (Facebook Inc n.d.).
     */
    decrementTime() {
        this.setState({
            time: this.state.time - 1
        });

        if(this.state.time === 0){
            this.intervals.map(clearInterval);
            if(this.props.type === 'redirection') {
                Router.go(this.props.typeParams.routeRedirected, this.props.typeParams.routeParams);
            }
        }

    },


    /**
     * [[Defines what is rendered]]
     * @returns {[[HTML]]} [[AN HTML COMPONENT]]
     */
    render() {

        var timerPrefix ="", timerSuffix="";

        switch(this.props.type) {

            case "redirection":

/*				[[TimerPrefix]]*/
                timerPrefix += "You will be redirected to the " +
                                this.props.typeParams.pageName +
                                " page in ";

/*				[[TimerSuffix]]*/
                timerSuffix += " seconds."

                break;
            case "other":
                break;
            default:
                break;
        }

        return (
            <span>{timerPrefix}{this.state.time}{timerSuffix}</span>
        );
    }




});
