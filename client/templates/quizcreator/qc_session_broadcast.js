// Called before the rendering of the qcSessionBroadcast template
Template.qcSessionBroadcast.onCreated(function () {

    var instance = this;

    instance.SBcontentType = new ReactiveVar("");
    instance.SBdataType = new ReactiveVar("");

    instance.autorun(function () {


        var localSession = Template.currentData().session;

        //        [[1. If session hasn't been launched yet]]
        if (!localSession.launchedAt && !localSession.state) {


            //            CONTENT TYPE: WELCOME
            if (instance.SBcontentType.get() !== CqCommonConsts.SBcontentType.WELCOME) {
                instance.SBcontentType.set(CqCommonConsts.SBcontentType.WELCOME);
            }
            //            DATA TYPE: SESSION_AND_QUIZ
            if (instance.SBdataType.get() !== CqQcConsts.SBdataType.SESSION_AND_QUIZ) {
                instance.SBdataType.set(CqQcConsts.SBdataType.SESSION_AND_QUIZ);
            }

            //        [[2. If the session has been launched / is in progress]]
        } else if (localSession.launchedAt && localSession.state) {


            //            [[2.1. Content of type Question is needed]]
            if (localSession.state.type === 'q') {


                //            CONTENT TYPE: QUESTION
                if (instance.SBcontentType.get() !== CqCommonConsts.SBcontentType.QUESTION) {
                    instance.SBcontentType.set(CqCommonConsts.SBcontentType.QUESTION);
                }
                //            DATA TYPE: SESSION_QUIZ_AND_SQR
                if (instance.SBdataType.get() !== CqQcConsts.SBdataType.SESSION_QUIZ_AND_SQR) {
                    instance.SBdataType.set(CqQcConsts.SBdataType.SESSION_QUIZ_AND_SQR);
                }


                //            [[2.2. Content of type Result is needed]]
            } else if (localSession.state.type === 'r') {


                //            CONTENT TYPE: RESULT
                if (instance.SBcontentType.get() !== CqCommonConsts.SBcontentType.RESULT) {
                    instance.SBcontentType.set(CqCommonConsts.SBcontentType.RESULT);
                }
                //            DATA TYPE: SESSION_QUIZ_AND_SQR
                if (instance.SBdataType.get() !== CqQcConsts.SBdataType.SESSION_QUIZ_AND_SQR) {
                    instance.SBdataType.set(CqQcConsts.SBdataType.SESSION_QUIZ_AND_SQR);
                }

            }



            //        [[3. If the session has reached its end]]
        } else if (localSession.launchedAt && !localSession.state) {


            //            [[3.1. Content of type End (End page showing thanks) is needed]]
            if (!localSession.finishedAt) {


                //            CONTENT TYPE: END
                if (instance.SBcontentType.get() !== CqCommonConsts.SBcontentType.END) {
                    instance.SBcontentType.set(CqCommonConsts.SBcontentType.END);
                }
                //            DATA TYPE: SESSION_QUIZ_AND_SQR
                if (instance.SBdataType.get() !== CqQcConsts.SBdataType.SESSION_QUIZ_AND_SQR) {
                    instance.SBdataType.set(CqQcConsts.SBdataType.SESSION_QUIZ_AND_SQR);
                }

                /*            [[3.2. Content of type Finished (page with Timer that redirects to the
                            Session page where the results are available) is needed]]*/
            } else {


                //            CONTENT TYPE: FINISHED
                if (instance.SBcontentType.get() !== CqCommonConsts.SBcontentType.FINISHED) {
                    instance.SBcontentType.set(CqCommonConsts.SBcontentType.FINISHED);
                }
                //            DATA TYPE: SESSION_QUIZ_AND_SQR
                if (instance.SBdataType.get() !== CqQcConsts.SBdataType.SESSION_ONLY) {
                    instance.SBdataType.set(CqQcConsts.SBdataType.SESSION_ONLY);
                }
            }


        }

        var subscription = instance.subscribe('qcSessionBroadcastDynaPub',
            localSession._id,
            instance.SBdataType.get());

    });

});


/*For each question, we go through 2 state types/steps:
    - 'q', the first, tells that the question with its possible answers have to be rendered
    - 'r', the second, tells that the question results have to be rendered

state.index is just the index of the current question shown */

Template.qcSessionBroadcast.onRendered(function () {


    var instance = this;

    //    Dynamic template region = qcCurrentContent
    instance.autorun(function () {

        //        [[Reactive var that triggers the Re-run of the Autorun block]]
        var sbContentType = instance.SBcontentType.get();


        if (instance.subscriptionsReady()) {

            /*        The possible values have to reflect the ones available inside
                    /packages/ddzfred:cq.tools/cqCommonConsts.js. See object CqCommonConsts.*/
            switch (sbContentType) {

                //            [[CONTENT TYPE: WELCOME]]
                case "w":
                    Router.current().render('commonSessionBroadcastWelcome', {
                        to: 'qcCurrentContent',
                        data: {
                            title: Quizzes.findOne().title,
                            subtitle: Quizzes.findOne().subtitle,
                            author: Quizzes.findOne().author
                        }
                    });
                    break;


                    //            [[CONTENT TYPE: QUESTION]]
                case "q":
                    Router.current().render('qcSessionBroadcastQuestionContent', {
                        to: 'qcCurrentContent',
                        data: {
                            title: Quizzes.findOne().title,
                            subtitle: Quizzes.findOne().subtitle,
                            qNumber: Template.currentData().session.state.index + 1,
                            qCount: Quizzes.findOne().questions.length,
                            question: Quizzes.findOne().questions[Template.currentData().session.state.index]
                        }

                    });
                    break;


                    //            [[CONTENT TYPE: RESULT]]
                case "r":
                    Router.current().render('qcSessionBroadcastQuestionResults', {
                        to: 'qcCurrentContent',
                        data: {
                            title: Quizzes.findOne().title,
                            subtitle: Quizzes.findOne().subtitle,
                            qNumber: Template.currentData().session.state.index + 1,
                            qCount: Quizzes.findOne().questions.length,
                            question: Quizzes.findOne().questions[Template.currentData().session.state.index]
                        }
                    });
                    break;


                    //            [[CONTENT TYPE: END]]
                case "e":
                    Router.current().render('qcSessionBroadcastEnd', {
                        to: 'qcCurrentContent',
                        data: {

                        }
                    });
                    break;


                    //            [[CONTENT TYPE: FINISHED]]
                case "f":
                    Router.current().render('qcSessionBroadcastFinished', {
                        to: 'qcCurrentContent',
                        data: {
                            sessionId: Template.currentData().session._id
                        }
                    });
                    break;
                default:
                    break;

            }

        } else {
            Router.current().render('commonSessionBroadcastLoadingData', {
               to: 'qcCurrentContent'
            });
        }

    });


});

Template.qcSessionBroadcast.helpers({

});

Template.qcSessionBroadcast.events({

    'click .goToSessionPageButton': function (e) {

        e.preventDefault();

        Router.go('qcSessionPage', {
            _id: this.session._id
        });

    },

    //    LAUNCH SESSION BUTTON
    'click .sessionLaunchBtn': function (e) {

        _ = lodash;

        e.preventDefault();

        Meteor.call('sessionLaunchUpdate',
            Template.currentData().session._id,
            function (error, result) {
                if (error) return console.log("METHOD ERROR: " + error.reason);
            });

    },


    //    NEXT CONTENT BUTTON
    'click .sessionNextBtn': function (e) {

        e.preventDefault();

        var actualState = Template.currentData().session.state;
        var qCount = Quizzes.findOne().questions.length;


        //        If current content shown = Question
        if (actualState.type === 'q') {
            //            Then, go to the current question results
            Meteor.call('sessionGoToQuestionResultsUpdate',
                Template.currentData().session._id,
                function (error, result) {
                    if (error) return console.log("METHOD ERROR: " + error.reason);
                });

            //        if current content shown = Results
        } else if (actualState.type === 'r') {



            //            if the results shown are not from the last question, then, go to the next question
            if (actualState.index < qCount - 1) {

                Meteor.call('sessionGoToNextQuestionUpdate',
                    Template.currentData().session._id,
                    function (error, result) {
                        if (error) return console.log("METHOD ERROR: " + error.reason);
                    });

                //            If it's the results of the last question, then go to the End Page
            } else {

                Meteor.call('sessionGoToEndUpdate',
                    Template.currentData().session._id,
                    function (error, result) {
                        if (error) return console.log("METHOD ERROR: " + error.reason);
                    });
            }
        }

    },


    //    PREVIOUS CONTENT BUTTON
    'click .sessionPreviousBtn': function (e) {

        e.preventDefault();

        var actualState = Template.currentData().session.state;
        var qCount = Quizzes.findOne().questions.length;


        if (actualState) {


            //If current content shown = Question
            if (actualState.type === 'q') {

                if (actualState.index > 0) {

                    /*                Call method to go to the result page of the previous question.
                                    - Decrement index
                                    - Change type to result */
                    Meteor.call('sessionReturnToPreviousQuestionResult',
                        Template.currentData().session._id,
                        function (error, result) {
                            if (error) return console.log("METHOD ERROR: " + error.reason);
                        });

                }

            } else if (actualState.type === 'r') {

                //            Call method to just change the state type to question.
                Meteor.call('sessionReturnToQuestionContent',
                    Template.currentData().session._id,
                    function (error, result) {
                        if (error) return console.log("METHOD ERROR: " + error.reason);
                    });

            }


            /*            If actualState is null - meaning that we are at the End page (that shows thanks..)
                        then, go to the last question results */
        } else {
            Meteor.call('sessionReturnToLastQuestionResults',
                Template.currentData().session._id,
                qCount,
                function (error, result) {
                    if (error) return console.log("METHOD ERROR: " + error.reason);
                });
        }
    },


    //    FINISH SESSION BUTTON
    'click .sessionFinishBtn': function (e) {

        e.preventDefault();

        Meteor.call('sessionFinishUpdate',
            Template.currentData().session._id,
            function (error, result) {
                if (error) return console.log("METHOD ERROR: " + error.reason);
            });
    }

});
