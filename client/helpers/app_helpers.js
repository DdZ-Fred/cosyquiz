// Global helpers


/************************************************************/
/* INFO: CODE TAKEN FROM the following location:
    http://evgenys-blog.blogspot.co.uk/2014/06/index-in-each-expression-meteor.html
    The code-block was written by Jenia Nemzer (2014).
**************************************************************/
 Template.registerHelper('indexedCursor', function (context, options) {
     if(context) {
         return context.map(function(item, index) {
             item._index = index + 1;
             return item;
         });
     }
});
/************************************************************/

Template.registerHelper('DateFormat', function(jsDate) {
	return moment(jsDate).format("DD/MM/YYYY, HH:mm");
});