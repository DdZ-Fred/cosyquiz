/*INFO: Sign - in & Log - in Config*/
/* Config for the Sign-up/Login/Logout functionalities brought by
 the packages:  
    - ian:accounts-ui-bootstrap-3
    - accounts-password 
 
 */
Accounts.ui.config({
    passwordSignupFields: 'USERNAME_AND_EMAIL',
    extraSignupFields: [{
        fieldName: 'firstname',
        fieldLabel: 'First name',
        inputType: 'text',
        visible: true,
        validate: function (value, errorFunction) {
            if (!value) {
                errorFunction("Please enter your first name");
                return false;
            } else {
                return true;
            }
        }
    }, {
        fieldName: 'lastname',
        fieldLabel: 'Last name',
        inputType: 'text',
        visible: true,
        validate: function (value, errorFunction) {
            if (!value) {
                errorFunction("Please enter your last name");
                return false;
            } else {
                return true;
            }
        }
    }, {
        fieldName: 'gender',
        fieldLabel: 'Gender',
        showFieldLabel: true,
        inputType: 'radio',
        radioLayout: 'inline',
        data: [{
            id: 1,
            label: 'Female',
            value: 'f',
            checked: 'checked'
            }, {
            id: 2,
            label: 'Male',
            value: 'm'
            }],
        visible: true
    }, {
        fieldName: 'role',
        fieldLabel: 'Role',
        showFieldLabel: true,
        inputType: 'radio',
        visible: true,
        radioLayout: 'vertical',
        data: [{
            id: 1,
            label: 'Audience',
            value: 'au',
            checked: 'checked'
        }, {
            id: 2,
            label: 'Quiz Creator',
            value: 'qc'
        }]
    }, {
        fieldName: 'terms',
        fieldLabel: 'I accept the terms and conditions',
        inputType: 'checkbox',
        visible: true,
        saveToProfile: false,
        validate: function (value, errorFunction) {
            if (!value) {
                errorFunction("You must accept the terms and conditions");
                return false;
            } else {
                return true;
            }
        }
    }]
});


/* INFO: this
function returns the 'options'
object that is later used by Accounts.onCreateUser callback on the server
- See /server/lib/environment.js  */
accountsUIBootstrap3.setCustomSignupOptions = function () {

    return {
        firstname: Session.get('firstname'),
        lastname: Session.get('lastname'),
        gender: Session.get('gender'),
        role: Session.get('role')
    };
};

/*INFO: Code run when logged out successfully*/
accountsUIBootstrap3.logoutCallback = function (error) {
    if (error) { return console.log("Error:" + error.reason);}
    Router.go('root');

};
