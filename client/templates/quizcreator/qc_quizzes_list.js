Template.qcQuizzesList.helpers({
    quizzes: function () {
        return Quizzes.find({}, {
            sort: {
                createdAt: -1
            }
        });
    },

    emptyQuizzesList: function () {
        return Quizzes.find().count() === 0;
    }
});
