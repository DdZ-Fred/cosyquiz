Gathers the logic that allows to process an Audience-User's answer and its correctness (his result) in order to show the appropriate content.


[[RULES/CONVENTIONS]]
- One File = One Question-Type
- Each and every function MUST be named accordingly to the actual User-Type() and Question-Type by including prefixes
Example: auMcFunctionOneName() the User-Type Audience(au) and Question-Type Multiple-Choice(Mc)
