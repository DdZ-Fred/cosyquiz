Template.auSessionBroadcastQuestionForm.onRendered(function () {

    React.render(
        React.createElement(QuestionContentAU, {
            question: Template.currentData().question,
            sessionId: Template.currentData().sessionId,
            qIndex: Template.currentData().qNumber - 1
        }),
        document.getElementById('questionFormAnchor')
    );
});
