QuestionContentAU = React.createClass({

    mixins: [ReactMeteorData, AuConProc_MultipleChoice],

    getMeteorData() {
        return {
            asr: AudienceSessionResults.findOne()
        }
    },

    propTypes: {
        question: React.PropTypes.object.isRequired,
        sessionId: React.PropTypes.string.isRequired,
        qIndex: React.PropTypes.number.isRequired
    },

    isAnswered() {
        return this.data.asr.questions[this.props.qIndex].answered;
    },

    renderButton() {
        if(this.isAnswered()) {
            return  <button type="button"
                            className="btn btn-success btn-lg btn-block"
                            disabled="disabled">Answer received!</button>;
        } else {
            return  <button type="submit"
                            className="btn btn-default btn-lg btn-block"
                            onClick={this.sendAnswer}>Send</button>;
        }
    },

    /**
    * TYPED-CONTENT
    * Renders the appropriate HTML content depending on the question type.
    * @returns {HTML} [[The appropriate HTML Block]]
    */
    renderTypedContent() {

        switch(this.props.question.qType) {

/*            [[MULTIPLE-CHOICE]]*/
            case 'multiple_choice':

                if(this.isAnswered()) {
                    return this.auConMc(this.props.question.qAnswers,
                                        this.data.asr.questions[this.props.qIndex].choices);
                } else {
                    return this.auConMc(this.props.question.qAnswers);
                }
                break;

            case 'other':
                break;
            default:

                break;

        }

    },

    /**
     * Sends the Audience-User answer to the server
     * @param {[[JS Event]]} e [[Click event. See renderButton() function above.]]
     */
    sendAnswer(e) {

        e.preventDefault();

        var newParticipant = CqAuFuncs.scanQuestionAnswer(this.props.question.qType);

        if(newParticipant) {


/*            [[Meteor method call #1]]*/
            Meteor.call('sqrUpdateWithAudienceAnswer',
                {
                    sessionId: this.props.sessionId,
                    questionId: this.props.question._id,
                    questionType: this.props.question.qType
                },
                newParticipant,
                function(error, result) {
                    if(error) {return console.log("METHOD ERROR: " + error.reason);}
            });


/*            [[The method #1 above contains at its start (see /lib/collections/session_question_results.js)
            a call of the function 'this.unblock()'. Using it allows the code to be run asynchronously in separate NodeJS Fiber(similar to a Thread)
            Thus, we unblock the following DDP Messages - other Meteor method calls and Subscriptions that follow - and
            the Meteor method call #2 below is executed almost at the same time.]]*/



/*            [[Meteor method call #2]]*/
            Meteor.call('asrUpdateWithQuestionAnswer',
                {
                    sessionId: this.props.sessionId,
                    questionId: this.props.question._id,
                    questionType: this.props.question.qType,
                    questionIndex: this.props.qIndex
                },
                newParticipant,
                function(error, result) {
                    if(error) {return console.log("METHOD asrUpdateWithQuestionAnswer ERROR: " + error.reason);}
            });

        } else {
/*			[[SHOW NOTIFICATION CUSTOMIZED BY TYPE]] */
            alert("No answer has been defined/selected")
        }

    },

    render() {

        return (
            <div className="panel panel-default">
                <p className="text-center">{this.props.question.qTitle}</p>
                <br />
                <br />
                <form className="auQuestionForm">
                    {this.renderTypedContent()}
                    <br />
                    {this.renderButton()}
                </form>
            </div>
        )
    }

});
