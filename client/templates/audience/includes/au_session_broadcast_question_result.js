Template.auSessionBroadcastQuestionResult.onRendered(function() {


    React.render(
        React.createElement(QuestionResultAU, {
            question: Template.currentData().question,
            qIndex: Template.currentData().qNumber - 1
        }),
        document.getElementById('questionResultAnchor')
    );

});
