Template.notFound.onRendered(function() {
    React.render(
        React.createElement(Timer, {
                                initialTime: 4,
                                type: 'redirection',
                                typeParams: {
                                    routeRedirected: 'root',
                                    routeParams: {},
                                    pageName: 'Home'
                                }
                            }
        ),
        document.getElementById('notFoundTimerAnchor')
    );
});
