Gathers the logic that allows to process a question results of the whole audience and arrange it appropriately for the QuizCreator-User need.


[[RULES/CONVENTIONS]]
- One File = One Question-Type
- Each and every function MUST be named accordingly to the actual User-Type() and Question-Type by including prefixes
Example: auMcFunctionOneName() for the User-Type Audience(au) and Question-Type Multiple-Choice(Mc)
