Package.describe({
    name: "ddzfred:cq.tools.qc-content-processing",
    summary: "Gathers the logic that allows to process a question's content appropriately",
    version: "1.0.0",
    documentation: "README.md"
});

Package.onUse(function(api) {

//    [[Dependencies]]
    api.use("jsx", "client");
    api.use("twbs:bootstrap@3.3.5", "client");

//    [[Packages files and exports]]
    api.add_files("qcConProc_MultipleChoice.jsx", "client");
    api.export("QcConProc_MultipleChoice", "client");

});
