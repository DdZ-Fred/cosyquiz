//Called when the template is inserted into the DOM (good for DOM manipulations)
Template.qcQuizPage.onRendered(function () {

    var $questionAnchor = $("#questionAnchor");

    // Generate form from quiz data
    CqQcFuncs.quiz.generateFormFromData(Template.currentData().quiz, $questionAnchor);
    CqQcFuncs.quiz.updateQuestionsIndex();

});

Template.qcQuizPage.helpers({
    questionsCount: function () {
        return Session.get('questionsCount');
    },

    questionTypesList: function () {
        return CqCommonFuncs.getQuestionTypes();
    }
});

Template.qcQuizPage.events({


    // ADD QUESTION BUTTON
    'click .addQuestionButton': function (e) {

        e.preventDefault();

        var $questionAnchor = $("#questionAnchor");
        var type = $("div.newQuestionDiv").find("[name=selectQuestionType]").val();

        var $newQuestion = CqQcFuncs.quiz.createQuestionItem(type);
        $newQuestion.insertBefore($questionAnchor);

        Session.set("questionsCount", Session.get("questionsCount") + 1);

        CqQcFuncs.quiz.updateQuestionsIndex();


    },


    //    REMOVE QUESTION BUTTON
    'click .removeQuestionButton': function (e) {

        e.preventDefault();

        $(e.target).closest("div.questionItem").remove();

        Session.set("questionsCount", Session.get("questionsCount") - 1);
        CqQcFuncs.quiz.updateQuestionsIndex();

    },


    //    ADD ANSWER BUTTON
    'click .addAnswerButton': function (e) {

        e.preventDefault();

        var $questionAnswersTable =
            $(e.target).closest("table.questionAnswers");

        var $answerAnchor =
            $questionAnswersTable.find("tr.answerAnchor");

        var $answerItem = CqQcFuncs.quiz.createAnswerItem("multiple_choice");
        $answerItem.insertBefore($answerAnchor);

        CqQcFuncs.quiz.updateAnswersIndex($questionAnswersTable);

    },


    //    REMOVE ANSWER BUTTON
    'click .removeAnswerButton': function (e) {

        e.preventDefault();

        var $questionAnswersTable =
            $(e.target).closest("table.questionAnswers");


        $(e.target).closest("tr.answerItem").remove();

        CqQcFuncs.quiz.updateAnswersIndex($questionAnswersTable);

    },


    //    ACTIVATE QUIZ EDITION
    'click a.quizEditButton': function (e) {

        e.preventDefault();

        $("fieldset").prop("disabled", false);

    },


    //    SAVE QUIZ MODIFICATIONS
    'submit form': function (e) {

        e.preventDefault();

        var updatedQuiz = CqQcFuncs.quiz.scanQuizForm();

        Meteor.call('quizUpdate', this.quiz._id, updatedQuiz, function (error, result) {
            if (error) {console.log("METHOD ERRROR: " + error.reason);}
        });

        $("fieldset").prop("disabled", true);

    },


    //    DELETE QUIZ
    'click a.quizDeleteButton': function (e) {

        e.preventDefault();

        Meteor.call('quizRemove', this.quiz._id, function (error, result) {
            if (error) {console.log("METHOD ERROR: " + error.reason);}
        });

        Router.go("qcQuizzesList");
    }
});
