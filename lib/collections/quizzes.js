/*****************************/
/*** COLLECTION DEFINITION ***/
/*****************************/

Quizzes = new Mongo.Collection('quizzes');





/**********************/
/***  DEPENDENCIES  ***/
/**********************/





/*********************/
/***     RULES     ***/
/*********************/






/*********************/
/***    METHODS    ***/
/*********************/

Meteor.methods({

	/**
	 * INSERTS NEW QUIZ DOCUMENT IN DB
	 * @param   {Object} quiz [[Represents the quiz object/document]]
	 * @returns {Object} A single-property object containing the _id of the newly inserted document
	 */
	quizInsert: function (quiz) {

		check(Meteor.userId(), String);
		// ADD COMPLETE QUIZ CHECK HERE


		var user = Meteor.user();

		if (user && user.role === "quizcreator") {

			var finalQuiz = _.extend(quiz, {
				createdAt: new Date(),
				userId: user._id,
				author: user.profile.firstname + " " + user.profile.lastname,
				sessionsCount: 0,
				totalSessionsCreated: 0
			});

			var quizId = Quizzes.insert(finalQuiz);

			return {
				_id: quizId
			};

		} else {
			throw new Meteor.Error("Invalid user", "Not logged or/and not a QuizCreator");
		}
	},



	/**
	 * UPDATES THE QUIZ DOCUMENT IN DB
	 * @param   {String} quizId Represents the _id of the quiz object. Used to retrieve the object.
	 * @param   {Object} quiz   Represents the quiz object/document with its updated properties
	 * @returns {Number} [[Number 1 if a document has been updated, otherwise 0]]
	 */
	quizUpdate: function (quizId, quiz) {

		check(quizId, String);
		//		ADD COMPLETE QUIZ CHECK HERE


		var user = Meteor.user();

		if (user && user.role === 'quizcreator') {

			var oldQuiz = Quizzes.findOne({
				_id: quizId
			});
			if (oldQuiz) {

				var update = Quizzes.update({
					_id: quizId
				}, {
					$set: {
						title: quiz.title,
						subtitle: quiz.subtitle,
						codePrefix: quiz.codePrefix,
						questions: quiz.questions
					}
				});

				return update;

			} else {
				throw new Meteor.Error("Invalid quiz", "Quiz object couldn't be found");
			}

		} else {
			throw new Meteor.Error("Invalid user", "Not logged or/and not a QuizCreator");
		}

	},
	
	
	quizUpdateForSessionCreation: function(quizId) {
		
		check(quizId, String);
		
		var user = Meteor.user();
		if((user && user.role === 'quizcreator') || Meteor.isServer) {
			
			var update = Quizzes.update({
				_id: quizId
			}, {
				$inc: {
					sessionsCount: 1,
					totalSessionsCreated: 1
				}
			});
			
			return update;
			
		} else {
			throw new Meteor.Error("Invalid requester", "Not called by a QuizCreator or from the Server");
		}
		
	},
	
	
	quizUpdateForSessionDeletion: function(quizId) {
	
		check(quizId, String);
		
		var user = Meteor.user();
		if(user && user.role === 'quizcreator') {
			
			var update = Quizzes.update({
				_id: quizId
			}, {
				$inc: {
					sessionsCount: -1
				}
			});
			
			return update;
			
		} else {
			throw new Meteor.Error("Invalid user", "Not logged or/and not a QuizCreator");
		}
		
	},


	/**
	 * REMOVES THE QUIZ DOCUMENT ASSOCIATED TO THE _ID GIVEN
	 * @param   {String} quizId [[_id property of the quiz document]]
	 * @returns {Object} [[Result of the remove operation]]
	 */
	quizRemove: function (quizId) {

		check(quizId, String);
		var user = Meteor.user();

		if (user && user.role === 'quizcreator') {
			var remove = Quizzes.remove({
				_id: quizId
			});
			return remove;
		} else {
			throw new Meteor.Error("Invalid user", "Not logged or/and not a QuizCreator");
		}
	}

});


// CUSTOM CHECKS

var localChecks = {

	QuizCheck: Match.Where(function (quiz) {
		check(quiz.title, String);
		check(quiz.subtitle, String);
		check(quiz.codePrefix, String);

		_.forEach(quiz.questions, function (q, qIndex) {
			check(q.qTitle, String);

		});

	})

};
