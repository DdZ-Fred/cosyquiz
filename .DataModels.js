var User = {
    _id: String,
    createdAt: Date (provided by external package),
    services: Object (provided by external package),
    username: String (provided by external package),
    emails: Array (provided by external package),
    profile: {    
        firstname: String,
        lastname: String,
        gender: String = m / f
    },
    role: String = superadmin / quizcreator / audience,
    activated: true / false,
}

var Quiz = {
    _id: String,
    createdAt: Date,
    userId: String,
    author: String = FullName,
    title: String,
    subtitle: String,
    codePrefix: String,
    questions: [
        {
            _id: String,
            qType: String,
            qTitle: String,
            qAnswers: [
                {
                    _id: String,
                    aLetter: String,
                    aTitle: String,
                    aCorrect: true / false
                }
            ]


        }
    ]

}

var Session = {
    _id: String,
    createdAt: Date,
    lauchedAt: Date,
    finishedAt: Date,
    userId: String,
    quizId: String,
    sessionCode: quiz.codePrefix + String,
    state: {
        type: String = "Question or Result? 'q' or 'r' ",
        index: 'questionIndex'
    }
}

var SessionQuestionResults = {
    _id: String,
    sessionId: String,
    quizId: String,
    userId: String,
    questionId: String,
    questionType: String,
    results: {
        participantsCount: number, //Incremented at each insertion
        participants: [
            {
                userId: String,
                choices: [aLetter, aLetter...] or [aId, aId...]
            }
        ]

    }
}

var AudienceSessionResults = {
	
	_id: String,
	sessionId: String,
	userId: String,
	questions: [
		
		
//		STRUCTURE/CONTENT WILL BE BE SIMILAR TO THE SESSION_QUESTION_RESULTS
		
//		[[MULTIPLE-CHOICE]]
		{
			questionId: String,
			answered: Boolean,
			choices: [] //containing the aLetters associated to the choices selected
		}
		
		
//		[[WORDS]]
		{
			questionId: String,
			answered: Boolean,
		}
		
		
	]
	
}
