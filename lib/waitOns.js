/*****************************/
/****   1. DEPENDENCIES   ****/
/*****************************/




var CQ_WaitOnFunctions = {



};



/*************************************/
/****   2. WAIT-ON APPLICATION   *****/
/*************************************/


/*************************/
/***  2.1. SUPERADMIN  ***/
/*************************/

Router.waitOn(function() {
   return Meteor.subscribe('desactivatedAccounts');
}, {
    only: 'saAccountRequests'
});





/*************************/
/**  2.2. QUIZCREATOR  ***/
/*************************/

Router.waitOn(function () {
    Session.set('questionsCount', 0);
}, {
    only: ['qcQuizCreate']
});

Router.waitOn(function () {

    var limit = parseInt(this.params.quizzesLimit) || 10;
    return Meteor.subscribe('quizzes', {
        sort: {
            createdAt: -1
        },
        limit: limit
    });

}, {
    only: 'qcQuizzesList'
});

Router.waitOn(function () {
    Session.set("questionsCount", 0);
    Session.set("quizId", this.params._id);
    return Meteor.subscribe('quiz', this.params._id);
}, {
    only: 'qcQuizPage'
});

Router.waitOn(function () {
    return Meteor.subscribe('QuizzesInfoForSession');
}, {
    only: 'qcSessionCreate'
});

Router.waitOn(function() {
    return Meteor.subscribe('qcSessionPageJointPub', this.params._id);
}, {
    only: 'qcSessionPage'
});

Router.waitOn(function () {

    var listingType = this.params.listingType;
    var limit = parseInt(this.params.sessionsLimit) || 10;

    return Meteor.subscribe('sessionsList', {
        listingType: listingType,
        limit: limit
    });


}, {
    only: 'qcSessionsList'
});

Router.waitOn(function () {

    return Meteor.subscribe('session', this.params._id, 'id');
}, {
    only: 'qcSessionBroadcast'
});





/***********************/
/***  2.3. AUDIENCE  ***/
/***********************/

Router.waitOn(function () {

    return Meteor.subscribe('session', this.params._id, 'id');
}, {
    only: 'auSessionBroadcast'
});
