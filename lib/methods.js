Meteor.methods({
    activateAccount: function (userId) {

        check(Meteor.userId(), String);


        //       The user we want to activate
        check(userId, String);
        var desactivatedUser = Meteor.users.findOne({
            _id: userId
        });
        if (!desactivatedUser) throw new Meteor.Error('invalid', 'User not found');

        var user = Meteor.user();

        if (user && user.role === "superadmin") {
            Meteor.users.update({
                _id: userId
            }, {
                $set: {
                    activated: true
                }
            });
        }

    },

});
