/**
 * INSERTS DEFAULT USERS AND QUIZZES WHEN NONE CAN BE FOUND
 * AT SERVER START.
 * Helpful when the DB needs to be cleaned for tests
 * */




/* Default users insertion */
if (Meteor.users.find().count() === 0) {

    var DB_Users = [

        /* 1 */
        {
            "_id": "3u3xMTKFeXsjpoFMk",
            "createdAt": new Date(),
            "services": {
                "password": {
                    "bcrypt": "$2a$10$2aBeZLaAF/dJ9tFfC4D7aeCx0JTS4.DhqdYTl0oHHD7BgL1CiVcNG"
                },
                "resume": {
                    "loginTokens": []
                }
            },
            "username": "sa1",
            "emails": [
                {
                    "address": "sa1@cosyquiz.uk",
                    "verified": true
                }
            ],
            "profile": {
                "firstname": "Fender",
                "lastname": "Stratocaster",
                "gender": "m"
            },
            "role": "superadmin",
            "activated": true
        },

        /* 2 */
        {
            "_id": "K3Ja9Ep64LRytnfn3",
            "createdAt": new Date(),
            "services": {
                "password": {
                    "bcrypt": "$2a$10$JuGfGU73NftJA6I6Gu2zNevlU5w7NmisWMLbZgX78ocKp7EuV4pbi"
                },
                "resume": {
                    "loginTokens": []
                }
            },
            "username": "sa2",
            "emails": [
                {
                    "address": "sa2@cosyquiz.uk",
                    "verified": true
                }
            ],
            "profile": {
                "firstname": "Frederic",
                "lastname": "Rey",
                "gender": "m"
            },
            "role": "superadmin",
            "activated": true
        },

        /* 3 */
        {
            "_id": "9XzQFH5rojJppWLh3",
            "createdAt": new Date(),
            "services": {
                "password": {
                    "bcrypt": "$2a$10$UmlfpQaOfT3dqVf0M58nbu0i6NhjSIs10qtG7sa5j/oFi90mFkKBu"
                },
                "resume": {
                    "loginTokens": []
                }
            },
            "username": "qc1",
            "emails": [
                {
                    "address": "qc1@cosyquiz.uk",
                    "verified": true
            }
        ],
            "profile": {
                "firstname": "Gandalf",
                "lastname": "The White",
                "gender": "m"
            },
            "role": "quizcreator",
            "activated": true
},

/* 4 */
        {
            "_id": "yjHJh2uDfHm75M8jc",
            "createdAt": new Date(),
            "services": {
                "password": {
                    "bcrypt": "$2a$10$irAdb3qMLUqf9Qua2eCLzOyoWez3M9kwY6PUNlXyxfOd0tNf5R30e"
                }
            },
            "username": "qc2",
            "emails": [
                {
                    "address": "qc2@cosyquiz.uk",
                    "verified": true
                        }
                    ],
            "profile": {
                "firstname": "Le Vilain",
                "lastname": "Mechant",
                "gender": "m"
            },
            "role": "quizcreator",
            "activated": true
},

/* 5 */
        {
            "_id": "KTp3sRmbNecWNBsmz",
            "createdAt": new Date(),
            "services": {
                "password": {
                    "bcrypt": "$2a$10$8wOM5x0tCkN0HwSjvWfTjOwHKSW3J2Crj4msyULRMO0Qm795EjHmC"
                }
            },
            "username": "qc3",
            "emails": [
                {
                    "address": "qc3@cosyquiz.uk",
                    "verified": true
                        }
                    ],
            "profile": {
                "firstname": "Gibson",
                "lastname": "Les Paul",
                "gender": "f"
            },
            "role": "quizcreator",
            "activated": true
},

/* 6 */
        {
            "_id": "TRFPYuovecimfKnCF",
            "createdAt": new Date(),
            "services": {
                "password": {
                    "bcrypt": "$2a$10$MDF73Z9i5lDBoGaftFIXDO2TgEFZCRJxeRv99qEqZT4UMeZqSTTAu"
                }
            },
            "username": "qc4",
            "emails": [
                {
                    "address": "qc4@cosyquiz.uk",
                    "verified": true
                        }
                    ],
            "profile": {
                "firstname": "Jack",
                "lastname": "Sparrow",
                "gender": "m"
            },
            "role": "quizcreator",
            "activated": true
},

/* 7 */
        {
            "_id": "pZWGeHXroGeXKSFD6",
            "createdAt": new Date(),
            "services": {
                "password": {
                    "bcrypt": "$2a$10$VWVKzZRH71EJpz2C6pQtUuLfNEl6zeQTJ6.dWsjKsCE6pzlsvPBja"
                },
                "resume": {
                    "loginTokens": []
                }
            },
            "username": "au1",
            "emails": [
                {
                    "address": "au1@cosyquiz.uk",
                    "verified": false
                        }
                    ],
            "profile": {
                "firstname": "Deborah",
                "lastname": "Wright",
                "gender": "f"
            },
            "role": "audience",
            "activated": true
},

/* 8 */
        {
            "_id": "CvcN4h2enQuk8cnCL",
            "createdAt": new Date(),
            "services": {
                "password": {
                    "bcrypt": "$2a$10$AwfoCewgwprh81AiK1oh3OQ.MK7vmnm/kzBj5Ia4C2ThoUt8Lbb.q"
                },
                "resume": {
                    "loginTokens": []
                }
            },
            "username": "au2",
            "emails": [
                {
                    "address": "au2@cosyquiz.uk",
                    "verified": false
                        }
                    ],
            "profile": {
                "firstname": "Ervin",
                "lastname": "Clarke",
                "gender": "m"
            },
            "role": "audience",
            "activated": true
},

/* 9 */
        {
            "_id": "hENdQZ6xvvKY4pdzs",
            "createdAt": new Date(),
            "services": {
                "password": {
                    "bcrypt": "$2a$10$Q40bk/Ct0lhbwV.Ci7t7G.VO6rOF929QJlU2XmYei8dJl7b3v0j4O"
                },
                "resume": {
                    "loginTokens": []
                }
            },
            "username": "au3",
            "emails": [
                {
                    "address": "au3@cosyquiz.uk",
                    "verified": false
                        }
                    ],
            "profile": {
                "firstname": "Cynthia",
                "lastname": "Holloway",
                "gender": "f"
            },
            "role": "audience",
            "activated": true
},

/* 10 */
        {
            "_id": "amFAvXfFR44WuZZKH",
            "createdAt": new Date(),
            "services": {
                "password": {
                    "bcrypt": "$2a$10$9ZKvxR3AXD.7UPR8hTkBDORelM3H8Khtk/lChtymI81mQZ4IofEs."
                },
                "resume": {
                    "loginTokens": []
                }
            },
            "username": "au4",
            "emails": [
                {
                    "address": "au4@cosyquiz.uk",
                    "verified": false
                        }
                    ],
            "profile": {
                "firstname": "Seth",
                "lastname": "Stevenson",
                "gender": "m"
            },
            "role": "audience",
            "activated": true
}

    ];

    _.forEach(DB_Users, function (user, key) {
        Meteor.users.insert(user);
    });

}


/*  Default quizzes insertion
    One quiz per QuizCreator is inserted
    (They all contain the same data)
*/
if(Quizzes.find().count() === 0) {

    var DB_Quizzes = [

        /* 1 */
        {
            "title" : "Lecture 1 - Real-time web-apps with JavaScript",
            "subtitle" : "An introduction to MeteorJS",
            "codePrefix" : "QC1_MTR1_",
            "questions" : [ 
                {
                    "_id" : Random.id(),
                    "qType" : "multiple_choice",
                    "qTitle" : "What language is MeteorJS based on?",
                    "qAnswers" : [ 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "A",
                            "aTitle" : "COBOL",
                            "aCorrect" : false
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "B",
                            "aTitle" : "JavaScript",
                            "aCorrect" : true
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "C",
                            "aTitle" : "PHP",
                            "aCorrect" : false
                        }
                    ]
                }, 
                {
                    "_id" : Random.id(),
                    "qType" : "multiple_choice",
                    "qTitle" : "What DBMS is used by default?",
                    "qAnswers" : [ 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "A",
                            "aTitle" : "MySQL",
                            "aCorrect" : false
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "B",
                            "aTitle" : "Oracle SQL",
                            "aCorrect" : false
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "C",
                            "aTitle" : "MongoDB",
                            "aCorrect" : true
                        }
                    ]
                }
            ],
            "createdAt" : new Date(),
            "userId" : "9XzQFH5rojJppWLh3",
            "author" : "Gandalf The White",
            "sessionsCount" : 0,
            "totalSessionsCreated" : 0
        },

        /* 2 */
        {
            "title" : "Lecture 1 - Real-time web-apps with JavaScript",
            "subtitle" : "An introduction to MeteorJS",
            "codePrefix" : "QC2_MTR1_",
            "questions" : [ 
                {
                    "_id" : Random.id(),
                    "qType" : "multiple_choice",
                    "qTitle" : "What language is MeteorJS based on?",
                    "qAnswers" : [ 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "A",
                            "aTitle" : "COBOL",
                            "aCorrect" : false
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "B",
                            "aTitle" : "JavaScript",
                            "aCorrect" : true
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "C",
                            "aTitle" : "PHP",
                            "aCorrect" : false
                        }
                    ]
                }, 
                {
                    "_id" : Random.id(),
                    "qType" : "multiple_choice",
                    "qTitle" : "What DBMS is used by default?",
                    "qAnswers" : [ 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "A",
                            "aTitle" : "MySQL",
                            "aCorrect" : false
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "B",
                            "aTitle" : "Oracle SQL",
                            "aCorrect" : false
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "C",
                            "aTitle" : "MongoDB",
                            "aCorrect" : true
                        }
                    ]
                }
            ],
            "createdAt" : new Date(),
            "userId" : "yjHJh2uDfHm75M8jc",
            "author" : "Le Vilain Mechant",
            "sessionsCount" : 0,
            "totalSessionsCreated" : 0
        },

        /* 3 */
        {
            "title" : "Lecture 1 - Real-time web-apps with JavaScript",
            "subtitle" : "An introduction to MeteorJS",
            "codePrefix" : "QC3_MTR1_",
            "questions" : [ 
                {
                    "_id" : Random.id(),
                    "qType" : "multiple_choice",
                    "qTitle" : "What language is MeteorJS based on?",
                    "qAnswers" : [ 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "A",
                            "aTitle" : "COBOL",
                            "aCorrect" : false
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "B",
                            "aTitle" : "JavaScript",
                            "aCorrect" : true
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "C",
                            "aTitle" : "PHP",
                            "aCorrect" : false
                        }
                    ]
                }, 
                {
                    "_id" : Random.id(),
                    "qType" : "multiple_choice",
                    "qTitle" : "What DBMS is used by default?",
                    "qAnswers" : [ 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "A",
                            "aTitle" : "MySQL",
                            "aCorrect" : false
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "B",
                            "aTitle" : "Oracle SQL",
                            "aCorrect" : false
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "C",
                            "aTitle" : "MongoDB",
                            "aCorrect" : true
                        }
                    ]
                }
            ],
            "createdAt" : new Date(),
            "userId" : "KTp3sRmbNecWNBsmz",
            "author" : "Gibson Les Paul",
            "sessionsCount" : 0,
            "totalSessionsCreated" : 0
        },

        /* 4 */
        {
            "title" : "Lecture 1 - Realtime web-apps with JavaScript",
            "subtitle" : "An introduction to MeteorJS",
            "codePrefix" : "QC4_MTR1_",
            "questions" : [ 
                {
                    "_id" : Random.id(),
                    "qType" : "multiple_choice",
                    "qTitle" : "What language is MeteorJS based on?",
                    "qAnswers" : [ 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "A",
                            "aTitle" : "COBOL",
                            "aCorrect" : false
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "B",
                            "aTitle" : "JavaScript",
                            "aCorrect" : true
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "C",
                            "aTitle" : "PHP",
                            "aCorrect" : false
                        }
                    ]
                }, 
                {
                    "_id" : Random.id(),
                    "qType" : "multiple_choice",
                    "qTitle" : "What DBMS is used by default?",
                    "qAnswers" : [ 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "A",
                            "aTitle" : "MySQL",
                            "aCorrect" : false
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "B",
                            "aTitle" : "Oracle SQL",
                            "aCorrect" : false
                        }, 
                        {
                            "_id" : Random.id(),
                            "aLetter" : "C",
                            "aTitle" : "MongoDB",
                            "aCorrect" : true
                        }
                    ]
                }
            ],
            "createdAt" : new Date(),
            "userId" : "TRFPYuovecimfKnCF",
            "author" : "Jack Sparrow",
            "sessionsCount" : 0,
            "totalSessionsCreated" : 0
        }
    ];

    _.forEach(DB_Quizzes, function(quiz, key) {
        Quizzes.insert(quiz);
    });

}