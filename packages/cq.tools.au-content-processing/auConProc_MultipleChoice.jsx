/**********************************************************************************************
* DESCRIPTION: This file gathers the logic needed to process/show the question content for a question of type:
* Multiple-Choice (Mc/mc/MC)
* auCon = AudienceContent
**********************************************************************************************/

AuConProc_MultipleChoice = {

    choiceSelectedOrNot(isSelected, choice) {
        if(isSelected) {
            return  <label>
                        {choice.aTitle}
                        <input 	className="choice"
                                type="checkbox"
                                value={choice.aLetter}
                                readOnly
                                checked />
                    </label>;
        } else {
            return  <label>
                        {choice.aTitle}
                        <input 	className="choice"
                                type="checkbox"
                                value={choice.aLetter}/>
                    </label>;
        }
    },

    renderChoice(choice, auChoices) {

        var choiceToRender;

        if(auChoices) {

/*            [[Choice was not selected]]*/
            if(auChoices.indexOf(choice.aLetter) === -1) {
                choiceToRender = this.choiceSelectedOrNot(false, choice);

/*            [[Choice was selected]]*/
            } else {
                choiceToRender = this.choiceSelectedOrNot(true, choice);
            }

        } else {
            choiceToRender = this.choiceSelectedOrNot(false, choice);
        }

        return choiceToRender;

    },

    renderChoices(qAnswers, auChoices) {

        if(auChoices) {
            return qAnswers.map((choice, cIndex) => {
                return  <div className="form-group" key={choice._id}>
                            {this.renderChoice(choice, auChoices)}
                        </div>
            });
        } else {
            return qAnswers.map((choice, cIndex) => {
                return  <div className="form-group" key={choice._id}>
                            {this.renderChoice(choice)}
                        </div>
            });
        }

    },


    /**
     * Renders the content of a question of type Multiple-Choice
     * @param   {Boolean} isAnswered [[Whether the question has been answered or not]]
     * @param   {Array}   qAnswers   [[Array that comes from the current question and that contains the possibles choices]]
     * @param   {Array}   auChoices  [[Represents the Audience-User answer, if there is one (can be null)]]
     * @returns {HTML}    [[HTML block showing the possible choices. If the question has been answered then the current user's answer
     *                    is visible and the form is disabled]]
     */
    auConMc(qAnswers, auChoices) {
        if(auChoices) {
            return  <fieldset disabled>
                        {this.renderChoices(qAnswers, auChoices)}
                    </fieldset>;
        } else {
            return this.renderChoices(qAnswers);
        }
    }

}

