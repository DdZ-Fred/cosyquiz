QcConProc_MultipleChoice = {

    renderQuestionChoices(question) {
        return question.qAnswers.map((choice) => {
            return 	<tr key={choice._id}>
                        <td className="text-right info">{choice.aLetter}:</td>
                        <td className="text-center info">{choice.aTitle}</td>
                    </tr>;
        });
    },


    qcConMc(question) {

        return  <table className="table">
            <tbody>
                    {this.renderQuestionChoices(question)}
            </tbody>
                </table>;

    }

}
