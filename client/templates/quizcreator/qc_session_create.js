Template.qcSessionCreate.onRendered(function () {

    var quizzes = Quizzes.find({}, {
        sort: {
            createdAt: -1
        }
    }).fetch();

    var $optionTemp = $("option.hidden");
    if (quizzes.length > 0) {

        _.forEach(quizzes, function (quiz, index) {
            $optionTemp.clone()
                .removeAttr("class")
                .attr("value", quiz._id)
                .text(quiz.title)
                .insertBefore($optionTemp);

            if (index === 0) {
                $("#sessionQuiz").val(quiz._id);
                $("#quizSubtitle").val(quiz.subtitle);
                $("#quizCodePrefix").text(quiz.codePrefix);
                $("[name=codeSuffix]").val(CqQcFuncs.session.getCodeSuffixString(quiz.totalSessionsCreated));


            }
        });

    } else {
        $optionTemp.clone()
            .removeAttr("class")
            .text("No quiz available")
            .insertBefore($optionTemp);
    }

});

Template.qcSessionCreate.helpers({

    quizzesCount: function () {
        return Quizzes.find().count();
    },

    getFormDisable: function() {
        if(!Quizzes.find().count()) {
            return "disabled";
        }
    }
});

Template.qcSessionCreate.events({

    //    UPDATE CODEPREFIX DIV WHEN SELECTED QUIZ CHANGES
    'change select': function (e) {

        e.preventDefault();

        //        Get the new value (quizId)
        var quizId = $(e.target).val();

        //        Find it
        var quiz = Quizzes.findOne({
            _id: quizId
        });

        //        Assign the codePrefix to the div quizCodePrefix created
        $("#quizSubtitle").val(quiz.subtitle);
        $("#quizCodePrefix").text(quiz.codePrefix);
        $("[name=codeSuffix]").val(CqQcFuncs.session.getCodeSuffixString(quiz.totalSessionsCreated));

    },


//    CREATE SESSION
    'submit form': function (e) {

        e.preventDefault();

        var session = CqQcFuncs.session.scanSessionForm();

        Meteor.call("sessionInsert", session, function (error, result) {
            if (error) return console.log("METHOD ERROR: " + error.reason);

                Router.go("qcSessionPage", {_id: result._id});
        });

    },

    'click .sessionCreateAndStart': function(e) {

        e.preventDefault();

        var session = CqQcFuncs.session.scanSessionForm();

        Meteor.call("sessionInsert", session, function (error, result) {
            if (error) return console.log("METHOD ERROR: " + error.reason);
            Router.go("qcSessionBroadcast", {_id: result._id});
        });

    }
});
