Package.describe({
    name: "ddzfred:cq.tools.au-result-processing",
    summary: "Gathers the logic that allows to process an Audience-User's answer and its correctness (his result)" +
            " in order to show the appropriate content.",
    version: "1.0.7",
    documentation: "README.md"
});

Package.onUse(function(api) {

//    [[Dependencies: Other packages the current package is dependent on]]
    api.use("jsx", "client");
    api.use("twbs:bootstrap@3.3.5", "client");
//    api.use("react","client");  NOT NEEDED YET


//    [[Package files and exports]]
//    Question type: MULTIPLE-CHOICE
    api.add_files("auResProc_MultipleChoice.jsx", "client");
    api.export("AuResProc_MultipleChoice", "client");

//    Question type: OTHER TYPE

});
