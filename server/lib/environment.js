/*INFO: Server side Account creation config
    - user param represents the user object with all the natively properties required for the user to login(username, email, password...)
    - options, other parameters added manually in the sign-up form.(firstname, lastname, gender and role)
    See /client/helpers/config.js 
    - Returns the user object with all its properties
*/
Accounts.onCreateUser(function (options, user) {

    //    Basic gender check function
    genderCheck = Match.Where(function (g) {
        check(g, String);
        return g.length === 1 && (g === 'f' || g === 'm')
    });

    //    Role check function
    roleCheck = Match.Where(function (r) {
        check(r, String);
        if (r === 'sa') {
            throw new Meteor.Error('Hacking attempt', 'This is very bad');
        } else if (r === 'qc' || r === 'au') {
            return true;
        } else {
            return false;
        }
    });

    //    Check values first
    if (options.profile) {
        check(options.profile.firstname, String);
        check(options.profile.lastname, String);
        check(options.profile.gender, genderCheck);

        check(options.profile.role, roleCheck);

        var userRole;
        if (options.profile.role === 'qc') {
            userRole = 'quizcreator';
        } else if (options.profile.role === 'au') {
            userRole = 'audience';
        }

        //        Update user object
        _.extend(user, {
            profile: {
                firstname: options.profile.firstname,
                lastname: options.profile.lastname,
                gender: options.profile.gender
            },
            role: userRole,
            activated: userRole === 'quizcreator' ? false : true
        });

        return user;

    } else {
        throw new Meteor.Error('Invalid user', 'user options empty');
    }


});

/*INFO: Called whenever there 's a login attempt.
    - attempt contains info such as( allowed, error, user, connection...)
    - Must return true/false or throw an error*/
Accounts.validateLoginAttempt(function (attempt) {

    if (attempt.allowed && attempt.user.activated) {
        return true;
    } else {
        return false;
    }
});
