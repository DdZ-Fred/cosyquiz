QcResProc_MultipleChoice = {

    categoryPercentage(categoryCount, totalCount) {
        if(totalCount) {
            return (categoryCount / totalCount) *100;
        } else {
            return 0;
        }
    },

    rightAnswerClass(correctness) {
        if(correctness) {
            return "success";
        } else {
            return "";
        }
    },

    generateResults(categories, totalCount) {

        return categories.map((category, cIndex) => {
        return      <tr className={this.rightAnswerClass(category.correct)}
                        key={Random.id()}>
                        <td>{category.name}</td>
                        <td>{category.correct.toString()}</td>
                        <td>{category.count}</td>
                        <td>{this.categoryPercentage(category.count, totalCount)}%</td>
                    </tr>
        });

    },

    identifyRightAnswer(qAnswers) {

        var rightAnswer = [];

        for(var i = 0; i < qAnswers.length; i++) {
            if(qAnswers[i].aCorrect) {
                rightAnswer.push(qAnswers[i].aLetter);
            }
        }

        return rightAnswer;

    },

    categorizeResults(qAnswers, qResults) {

        _ = lodash;
        var categories = [];

/*        [[Initialize array with right answer (right combination of choices)]]*/
        categories.push({
            name: this.identifyRightAnswer(qAnswers).toString(),
            correct: true,
            count: 0
        });


/*        [[Add and Categorize participants results]]*/
        for(var i = 0; i < qResults.participants.length; i++) {


            var tempIndex = _.findIndex(categories,
                                        'name',
                                        qResults.participants[i].choices.toString());

/*            [[If the current combination of choices doesn't exist yet, then create it]]*/
            if(tempIndex === -1) {
                categories.push({
                    name: qResults.participants[i].choices.toString(),
                    correct: false,
                    count: 1
                });

/*            [[Otherwise, find the category and increment the count]]*/
            } else {
                categories[tempIndex].count++;
            }

        }

        return categories;

    },

    qcResMc(qAnswers, qResults) {
        return  <table  className="table table-bordered">
            <tbody>
                        <tr className="info">
                            <th>Combinations</th>
                            <th>Correctness</th>
                            <th>Count</th>
                            <th>Percentage</th>
                        </tr>

                        {this.generateResults(this.categorizeResults(qAnswers, qResults), qResults.participantsCount)}

                        <tr>
                            <th>Total</th>
                            <td></td>
                            <td>{qResults.participantsCount}</td>
                            <td>100%</td>
                        </tr>
            </tbody>
                </table>;

    }

}
