Template.qcQuizCreate.helpers({

	questionsCount: function () {
		return Session.get('questionsCount');
	},
	questionTypesList: function() {
		return CqCommonFuncs.getQuestionTypes();
	}
});

Template.qcQuizCreate.events({


	'click .addQuestionButton': function(e) {

		e.preventDefault();

		var $questionAnchor = $("#questionAnchor");

//		Get the question type
		var type = $("div.newQuestionDiv").find("[name=selectQuestionType]").val();

//		FUNCTION THAT RETURNS THE RIGHT QUESTION ELEMENT USING "questionTypeClass"
//		AND ADD THE QUESTION ELEMENT BEFORE THE ANCHOR
		var $newQuestion = CqQcFuncs.quiz.createQuestionItem(type);
		$newQuestion.insertBefore($questionAnchor);

//		Increment questionsCount
		Session.set('questionsCount', Session.get('questionsCount') + 1);

		CqQcFuncs.quiz.updateQuestionsIndex();



	},


	//    REMOVE A QUESTION
	'click .removeQuestionButton': function (e) {

		e.preventDefault();

		$(e.target).closest("div.questionItem").remove();

		Session.set('questionsCount', Session.get('questionsCount') - 1);
		CqQcFuncs.quiz.updateQuestionsIndex();

	},


	//    ADD AN ANSWER
	'click .addAnswerButton': function (e) {

		e.preventDefault();

//		This element is needed to update the answers' indexes
		var $questionAnswersTable =
			$(e.target).closest("table.questionAnswers");

//		Find the answerAnchor
		var $answerAnchor =
			$questionAnswersTable.find("tr.answerAnchor");

		var $answerItem = CqQcFuncs.quiz.createAnswerItem("multiple_choice");
		$answerItem.insertBefore($answerAnchor);

		CqQcFuncs.quiz.updateAnswersIndex($questionAnswersTable);

	},


	//    REMOVE AN ANSWER
	'click .removeAnswerButton': function (e) {

		e.preventDefault();

		//        Needed as not possible to use e.target after the remove!
		var $questionAnswersTable =
			$(e.target).closest("table.questionAnswers");

		$(e.target).closest("tr.answerItem").remove();

		CqQcFuncs.quiz.updateAnswersIndex($questionAnswersTable);

	},

	//    SUBMIT NEW QUIZ
	'submit form': function (e) {

		e.preventDefault();

		var quiz = CqQcFuncs.quiz.scanQuizForm();

		Meteor.call('quizInsert', quiz, function (error, result) {

			if (error) {
				throw new Meteor.Error("Invalid quiz", "An error");
			} else {
				Router.go('qcQuizPage', {
					_id: result._id
				});
			}

		});

	}


});
