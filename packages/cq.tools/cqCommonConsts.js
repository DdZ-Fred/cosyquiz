/****************
 * ** GLOSSARY **
 * **************
 * 
 * - au/Au/AU 	--> Audience
 * - qc/Qc/QC 	--> QuizCreator
 * - SB			--> SessionBroadcast
 * 
 * ***************/




/***********************************************
 * GATHERS CONSTANTS THAT CAN BE USED EVERYWHERE
 * *********************************************/
CqCommonConsts = {
	
	
	/* SB for SessionBroadcast. These constants represent the different types of content possible in a SessionBroadcast page,
	auSessionBroadcast and qcSessionBroadcast */
	SBcontentType: {
		WELCOME: "w",
		QUESTION: "q",
		RESULT: "r",
		END: "e",
		FINISHED: "f"
	}
};