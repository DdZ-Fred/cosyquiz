/*************************************
 * README:
 *
 * GATHERS ALL THE PUBLICATION METHODS.
 * IT IS ORGANIZED BY COLLECTION NAME
 * WHEN POSSIBLE.
 * **********************************/





/************************
 * COLLECTION #1: USERS
 ***********************/


/**
 * Publishes the currently logged user data:
 * just his id and role
 * */
Meteor.publish("userData", function () {
    if (this.userId) {
        return Meteor.users.find({
            _id: this.userId
        }, {
            fields: {
                'role': 1
            }
        });
    } else {
        this.ready();
    }
});


/**
 * Publishes the amount of desactivated account
 * */
Meteor.publish("desactivatedAccountsCount", function() {

    var user = Meteor.users.findOne({
        _id: this.userId
    });

    if(user && user.role === 'superadmin') {
        var cursor = Meteor.users.find({
            activated: false
        });
        Counts.publish(this, 'desactivatedAccounts-count', cursor);

    } else {
        return [];
    }

});


/**
 * Publishes all the desactivated user accounts (QuizCreator accounts).
 * Used in the SuperAdmin environment
 * */
Meteor.publish("desactivatedAccounts", function () {

    var user = Meteor.users.findOne({
        _id: this.userId
    });

    //    If user connected and of type superadmin
    if (user && user.role === 'superadmin') {
        return Meteor.users.find({
            activated: false
        });
    } else {
        //      Declares that no data is being published
        return [];
    }
});





/**************************
 * COLLECTION #2: QUIZZES
/*************************/


/**
 * Publishes all or a subset (using the 'options' parameter) of the Quizzes
 * docs created by the currently logged QuizCreator-User.
 * @param   {Object}   options    [[Contains filters to sort and limit(the amount) the documents returned]]
 * */
Meteor.publish("quizzes", function (options) {

    var user = Meteor.users.findOne({
        _id: this.userId
    });


    // Should check the options object
    check(options, {
        sort: Object,
        limit: Number
    });


    if (user && user.role === 'quizcreator') {
        return Quizzes.find({
            userId: this.userId
        }, options);
    } else {
        // No data being published
        return [];
    }
});


/**
 * Similar to the previous publication method but
 * publishes only the Quizzes docs with the attributes/fields specified.
 * This publication is used in the Session Creation page.
 * */
Meteor.publish('QuizzesInfoForSession', function () {

    var user = Meteor.users.findOne({
        _id: this.userId
    });

    //    NO OPTIONS TO CHECK

    if (user && user.role === "quizcreator") {
        return Quizzes.find({
            userId: this.userId
        }, {
            fields: {
                _id: 1,
                title: 1,
                subtitle: 1,
                codePrefix: 1,
                createdAt: 1,
                totalSessionsCreated: 1
            },
            sort: {
                createdAt: -1
            }
        });
    } else {
        return [];
    }

});


/**
 *
 * Publishes a single Quiz document.
 * @param   {String}   id    [[_id associated to the needed Quiz document]]
 * */
Meteor.publish("quiz", function (id) {

    check(id, String);

    if (this.userId) {
        return Quizzes.find({
            _id: id
        });
    } else {
        return [];
    }
});





/***************************
 * COLLECTION #3: SESSIONS
/**************************/


/**
 * Publishes the Session doc associated to the given parameters.
 * A Session doc can be identified by two values, its:
 * - '_id'
 * - 'sessionCode'
 * and both are unique.
 * @param   {String}   paramType    [['id' or 'sessionCode']]
 * @param   {String}   paramValue   [[Associated value]]
 * */
Meteor.publish('session', function (paramValue, paramType) {

    check(paramValue, String);
    check(paramType, String);

    if (this.userId) {

        switch (paramType) {

            case 'id':
                return Sessions.find({
                    _id: paramValue
                });
            case 'sessionCode':
                return Sessions.find({
                    sessionCode: paramValue
                });
            default:
                return [];
        }
    } else {
        //        No data is being published
        return [];
    }

});


/**
 * Publishes a subset of the Session docs created by the
 * currently logged QuizCreator-user.
 * @param   {Object}   options   [[Contains the listing type and the limit of documents to return]]
 */
Meteor.publish('sessionsList', function (options) {

    check(options, {
        listingType: String,
        limit: Number
    });

    var user = Meteor.users.findOne({
        _id: this.userId
    });

    if (user && user.role === 'quizcreator') {


        switch (options.listingType) {


//            [[Sessions that haven't been launched yet]]
            case 'ready':

                return Sessions.find({
                    launchedAt: null,
                    userId: user._id
                }, {
                    fields: {
                        _id: 1,
                        sessionCode: 1,
                        createdAt: 1,
                        userId: 1
                    },
                    sort: {
                        createdAt: -1
                    },
                    limit: options.limit

                });


//            [[Sessions in progress]]
            case 'inProgress':
                return Sessions.find({
                    launchedAt: {
                        $ne: null
                    },
                    finishedAt: null,
                    userId: user._id
                }, {
                    fields: {
                        _id: 1,
                        sessionCode: 1,
                        createdAt: 1,
                        launchedAt: 1,
                        userId: 1
                    },
                    sort: {
                        launchedAt: -1
                    },
                    limit: options.limit
                });


//            [[Past/Finished Sessions]]
            case 'past':

                return Sessions.find({
                    finishedAt: {
                        $ne: null
                    }, // $ne = Not equal (MongoDB operator)
                    userId: user._id
                }, {
                    fields: {
                        _id: 1,
                        sessionCode: 1,
                        createdAt: 1,
                        launchedAt: 1,
                        finishedAt: 1,
                        userId: 1
                    },
                    sort: {
                        finishedAt: -1
                    },
                    limit: options.limit
                });

            default:
                return [];
        }


    } else {
        return [];
    }

});





/*******************************************
 * COLLECTION #4: SESSION QUESTION RESULTS
/******************************************/






/*******************************************
 * COLLECTION #5: AUDIENCE SESSION RESULTS
/******************************************/






/*****************************************
 * JOINS / MULTI-COLLECTION PUBLICATIONS
/****************************************/


/**
 * Publishes data needed for the qcSessionPage to be rendered properly:
 *   - The Session doc requested
 *   - Its associated Quiz
 *   - Its potential SessionQuestionResults docs
 * @param   {String}    sessionId    [[Id of the requested Session document]]
 */
Meteor.publish('qcSessionPageJointPub', function (sessionId) {

    check(sessionId, String);

    var user = Meteor.users.findOne({
        _id: this.userId
    });

    if (user && user.role === 'quizcreator') {

        var session = Sessions.find({
            _id: sessionId
        });

        var quiz = Quizzes.find({
            _id: session.fetch()[0].quizId
        });

        return [session,
                quiz,
                SessionQuestionResults.find({
                sessionId: sessionId
            })];

    } else {
        return [];
    }



});


/**
 * Publishes the data needed for the qcSessionBroadcast page to be rendered properly.
 * The qcSessionBroadcast page renders multiple contents dynamically and calls (subscribe) each time this
 * publication method to make sure only the needed data is returned. The set of documents returned can include:
 *   - The associated Quiz document
 *   - The SessionQuestionResults docs (one per question)
 * @param   {String}    sessionId    [[Id of the requested Session document]]
 * @param   {String}    dataType     [[Represents the set of documents that has to be available on the Client]]
 */
Meteor.publish('qcSessionBroadcastDynaPub', function(sessionId, dataType) {

    check(sessionId, String);
    check(dataType, String);

    var user = Meteor.users.findOne({
        _id: this.userId
    });

    if(user && user.role === 'quizcreator') {

        var session;


        /*		There is already a subscription returning the required Session doc and it can be found
        at the Route level of the qcSessionBroadcast page. See: /lib/waitOns.js, Section 2.1. QUIZCREATOR.
        So there is no need to include a Session cursor in what is returned.

        All the dataTypes available are listed in /packages/ddzfred:cq.tools/cqQcConsts.js
*/
        switch(dataType) {

                //            [[SESSION AND QUIZ docs should be available]]
            case "S_Q":

                session = Sessions.findOne({_id: sessionId});

                return Quizzes.find({
                    _id: session.quizId
                }, {
                    fields: {
                        sessionsCount: 0,
                        totalSessionsCreated: 0
                    }
                });


                //            [[SESSION, QUIZ AND SQR (SessionQuestionResults) docs should be available]]
            case "S_Q_SQR":

                session = Sessions.findOne({_id: sessionId});

                return [
                    Quizzes.find({
                        _id: session.quizId
                    }, {
                        fields: {
                            sessionsCount: 0,
                            totalSessionsCreated: 0
                        }
                    }),
                    SessionQuestionResults.find({
                        sessionId: sessionId,
                        quizId: session.quizId
                    })
                ];


                //            [[SESSION doc (only) should be available]]
            case "S":
                return [];
        }

    } else {
        return [];
    }


});


/**
 * Publishes the data needed for the auSessionBroadcast page to be rendered properly.
 * The auSessionBroadcast page renders multiple contents dynamically and calls (subscribe) each time this
 * publication method to make sure only the needed data is returned. The set of documents returned can include:
 *   - The associated Quiz document
 *   - A single AudienceSessionResults document (associated to the currently logged Audience-User)
 * @param   {String}    sessionId    [[Id of the requested Session document]]
 * @param   {String}    dataType     [[Represents the set of documents that has to be available on the Client]]
 */
Meteor.publish('auSessionBroadcastDynaPub', function (sessionId, dataType) {

    check(sessionId, String);
    check(dataType, String);

    var user = Meteor.users.findOne({
        _id: this.userId
    });

    if (user && user.role === 'audience') {

        var session, quizCursor, asr;

/*		There is already a subscription returning the required Session doc and it can be found
        at the Route level of the auSessionBroadcast page. See: /lib/waitOns.js, Section 2.2. AUDIENCE.
        Therefore there is no need to include a Session cursor in what is returned here.

        All the dataTypes available are listed in /packages/ddzfred:cq.tools/cqAuConsts.js
        */
        switch (dataType) {


//            [[SESSION AND QUIZ docs should be available]]
            case "S_Q":

                session = Sessions.findOne({
                    _id: sessionId
                });
                quizCursor = Quizzes.find({
                    _id: session.quizId
                }, {
                    fields: {
                        sessionsCount: 0,
                        totalSessionsCreated: 0
                    }
                });

                return quizCursor;


//            [[SESSION, QUIZ AND ASR (AudienceSessionResult) docs should be available]]
            case "S_Q_ASR":
                session = Sessions.findOne({
                    _id: sessionId
                });
                quizCursor = Quizzes.find({
                    _id: session.quizId
                }, {
                    fields: {
                        sessionsCount: 0,
                        totalSessionsCreated: 0
                    }
                });
                asr = AudienceSessionResults.findOne({
                    sessionId: sessionId,
                    userId: this.userId
                });

                if (!asr) {
                    
                    Meteor.call('asrInsert', sessionId, this.userId, function (error, result) {
                        if (error) return console.log("METHOD 'asrInsert' ERROR: " + error.reason);
                    });
                }

                return [quizCursor,
                        AudienceSessionResults.find({
                            sessionId: sessionId,
                            userId: this.userId
                        })
                       ];


//            [[SESSION Doc ONLY should be available]]
            case "S":
                return [];

            default:

                return [];

        }

    } else {
        return [];
    }
});
