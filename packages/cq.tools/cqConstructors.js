CQ_Constructs = {
    AnswerItem: function AnswerItem(answerType) {
        this._id = Random.id();
		
		switch(answerType) {
			case "multiple_choice":
				this.aLetter = '';
				this.aTitle = '';
				this.aCorrect = true;
				break;
			case "words":
				break;
			default:
				break;
		}
		
    },
    QuestionItem: function QuestionItem(questionType) {
        this._id = Random.id();
        this.qType = questionType;
        this.qTitle = '';
		
		switch(questionType) {
				
			case "multiple_choice":
				this.qAnswers = [];
				break;
			case "words":
				this.qAnswer = '';
				break;
			default:
				break;
				
		}
		


    },
    QuizItem: function QuizItem() {
        this.title = '';
        this.subtitle = '';
        this.codePrefix = '';
        this.questions = [];
    },
    SessionQuestionResults: function SessionQuestionResults(sessionId, quizId, questionId, questionType) {
        this.sessionId = sessionId;
        this.quizId = quizId;
        this.questionId = questionId;
        this.questionType = questionType;
        this.results = {
            participantsCount : 0,
            participants: []
        };
    },
    Participant: function Participant(questionType) {

        switch(questionType) {
            case "multiple_choice":
                this.choices = [];
                break;
            case "words":
                break;
            default:
                break;
        }

    }

};
